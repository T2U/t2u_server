REM @alandrieu 20-03-2015
REM "Team repo <https://bitbucket.org/T2U/t2u_server/overview>"
REM "Private repo <https://github.com/alandrieu>"

echo "Start Program Java t2u_server" > T2U_run.log
set JAVA_HOME="C:\Program Files\Java\jdk1.8.0_25"
set CURRENT_PATH=%CD%

cd t2u_server 
./gradlew.bat run 2>> %CURRENT_PATH%\T2U_run.log
REM %JAVA_HOME% -classpath "%CURRENT_PATH%\t2u_server\bin;%CURRENT_PATH%\t2u_server\lib\restlet-jse-2.3.1\lib\org.restlet.jar;%CURRENT_PATH%\t2u_server\lib\gson-2.3.1.jar;%CURRENT_PATH%\t2u_server\lib\junit-4.12.jar;%CURRENT_PATH%\t2u_server\lib\hamcrest-core-1.3.jar;%CURRENT_PATH%\t2u_server\lib\sqlite-jdbc-3.8.7.jar;%CURRENT_PATH%\t2u_server\lib\genson-0.98.jar;%CURRENT_PATH%\t2u_server\lib\selenium-server-standalone-2.45.0.jar;%CURRENT_PATH%\t2u_server\lib\selenium-java-2.45.0.jar" com.main.t2u_server.Principale
cd ..
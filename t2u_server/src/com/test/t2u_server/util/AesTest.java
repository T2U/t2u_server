package com.test.t2u_server.util;

import com.main.t2u_server.util.AES256;
import com.main.t2u_server.util.AESMessage;
import com.main.t2u_server.util.Sha512;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by DEVELOPPEUR on 12/08/2015.
 */
public class AesTest {

    private String salt_ = "8QGV1TTASOSZATSLAGRY3HBSALETSPY2EZ22VBGABWZGK34GCM6330XCNYONKJPA4VYOG01SYJPN17Y4YA2Y8F3PELOZJZMNXCD59J85E4N8YBCPQYJPMXDBBLNFMH2P";
    @Ignore
    @Test
    public void accountRegistrationTest() throws Exception {

        AES256 encryptor = new AES256();

        // DATA
        String clearPassword = "MY_ACCOUNT_PASSWORD";
        String clearKey = Sha512.getSha512("MY_MAIN_PASSWORD", salt_);

        // TEST 1 :
        AESMessage encryptValue = encryptor.encrypt(clearPassword, clearKey, salt_);

        assertEquals(clearPassword, encryptor.decrypt(encryptValue, clearKey, salt_));

        // TEST 2 :
        clearPassword = "MY_MAIN_PASSWORD";
        clearKey = "MY_ACCOUNT_PASSWORD";

        encryptValue = encryptPassowrd(clearPassword, clearKey, salt_);

        assertEquals(clearPassword, decryptPassord(encryptValue, clearKey, salt_));
    }

    private static AESMessage encryptPassowrd(String clearPassword, String clearKey, String salt){
        AES256 cryptor = new AES256();

        return cryptor.encrypt(clearPassword, Sha512.getSha512(clearKey, salt), salt);
    }

    private static String decryptPassord(AESMessage encrypPassword, String clearKey, String salt){
        AES256 cryptor = new AES256();

        return cryptor.decrypt(encrypPassword, Sha512.getSha512(clearKey, salt), salt);
    }
}

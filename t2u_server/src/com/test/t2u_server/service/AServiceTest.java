package com.test.t2u_server.service;

import com.main.t2u_server.controller.Context;

/**
 * Created by DEVELOPPEUR on 20/06/2015.
 */
public abstract class AServiceTest {
    protected Context context;

    public AServiceTest()
    {
        // Initialisation du context
        context = new Context();
    }
}
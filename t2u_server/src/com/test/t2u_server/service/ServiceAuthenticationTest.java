package com.test.t2u_server.service;

import com.main.t2u_server.bean.Account;
import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.bean.Token;
import com.main.t2u_server.controller.Context;
import com.main.t2u_server.dataSource.AccountDataSource;
import com.main.t2u_server.dataSource.TokenDataSource;
import com.main.t2u_server.service.ServiceAuthentication;
import com.main.t2u_server.util.Sha256;
import org.joda.time.DateTime;
import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by DEVELOPPEUR on 20/06/2015.
 */
public class ServiceAuthenticationTest extends AServiceTest {

    private ServiceAuthentication serviceAuthentication;

    public ServiceAuthenticationTest()
    {
       super();

        serviceAuthentication = new ServiceAuthentication();
    }

    /**
     * Vérifier la validité d'un token (existence en base et durée valide)
     * @throws Exception
     */
    @Ignore
    @Test
    public void checkTokenValidityTest() throws Exception {
        Token newToken = new Token(1);

        TokenDataSource tokenDsrc = new TokenDataSource();

        // Création d'un nouveau token
        tokenDsrc.Insert(newToken);

        // Token Valide
        assertTrue(serviceAuthentication.checkTokenValidity(newToken));

        // Token Incorrect
        newToken = new Token(1);
        assertFalse(serviceAuthentication.checkTokenValidity(newToken));

        // Manuellement invalider le Token
        DateTime _newExpireUtc = DateTime.now();

        _newExpireUtc.plusMinutes(newToken.getRefreshTokenLifeTime() + 1);
        newToken.setExpiresUtc(_newExpireUtc.toString());

        tokenDsrc.Update(newToken);
        assertFalse(serviceAuthentication.checkTokenValidity(newToken));

        // Vérification que le token timemout n'existe plus en base
        assertEquals(0, tokenDsrc.Select(newToken).size());
    }

    /**
     * Test de la validation d'un Account (Création d'un compte puis appel de la méthode de vérification)
     * @throws Exception
     */
    @Ignore
    @Test
    public void checkAccountTest() throws Exception {
        Account account = new Account();

        AccountDataSource accountDsrc = new AccountDataSource();

        account.setUsername("toto");
        account.setUuid(515);
        account.setEmail("toto@gmail.fr.fr");
        account.setPassword(Sha256.getSha256("azerty"));

        accountDsrc.Insert(account);

        account.setPassword("azerty");
        assertTrue(serviceAuthentication.checkAccount(account));

        account.setPassword("qwerty");
        assertFalse(serviceAuthentication.checkAccount(account));

        account = new Account();
        assertFalse(serviceAuthentication.checkAccount(account));
    }

}

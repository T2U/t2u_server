package com.test.t2u_server.dataSource;

import com.main.t2u_server.bean.Account;
import com.main.t2u_server.bean.AccountLog;
import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.controller.Context;
import com.main.t2u_server.dataSource.AccountLogDataSource;
import com.owlike.genson.Genson;
import com.test.t2u_server.dao.DaoTest;
import org.junit.Ignore;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertFalse;

/**
 * Created by DEVELOPPEUR on 08/08/2015.
 */
public class AccountLogDataSourceTest {
    protected Context t2uContext;
    protected AccountLogDataSource dsrc;
    protected Genson jsonBuilder;

    public void initalisation() throws Exception {
        // Initialisation du context
        t2uContext = DaoTest.t2uContext;

        if(t2uContext == null)
            t2uContext = new Context(true);


        this.jsonBuilder = new Genson();

        dsrc = new AccountLogDataSource();
    }

    @Ignore @Test
    public void selectTest() throws Exception {
        initalisation();

        Account account = new Account();

        AccountLog accountLog = new AccountLog();
        String json;

        account.setEmail("test02@t2u.com");

        accountLog.setAccount(account);

        List<IBean> lstAccount = dsrc.Select(accountLog);

        json = jsonBuilder.serialize(lstAccount);

        System.out.println( "RESULT:" + json);

        assertFalse(json.isEmpty());
    }
}

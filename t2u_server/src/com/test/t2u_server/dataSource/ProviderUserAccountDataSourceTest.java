package com.test.t2u_server.dataSource;

import com.main.t2u_server.bean.Account;
import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.bean.ProviderUserAccount;
import com.main.t2u_server.controller.Context;
import com.main.t2u_server.dataSource.ProviderUserAccountDataSource;
import com.main.t2u_server.service.ServiceProviderUserAccount;
import com.owlike.genson.Genson;
import com.test.t2u_server.dao.DaoTest;
import org.junit.Ignore;
import org.junit.Test;
import org.testng.annotations.BeforeTest;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/**
 * Created by DEVELOPPEUR on 27/07/2015.
 */
public class ProviderUserAccountDataSourceTest {

    protected Context t2uContext;
    protected ProviderUserAccountDataSource dsrc;
    protected Genson jsonBuilder;

    public void initalisation() throws Exception {
        // Initialisation du context
        t2uContext = DaoTest.t2uContext;

        if(t2uContext == null)
            t2uContext = new Context(true);

        this.jsonBuilder = new Genson();

        dsrc = new ProviderUserAccountDataSource();
    }

    @Ignore @Test
    public void selectTest() throws Exception {
        initalisation();

        ProviderUserAccount providerUserAccount = new ProviderUserAccount();

        Account accountUser = new Account();
        String json;

        //accountUser.setEmail("t2u@test.com");
        accountUser.setEmail("rootAdminUser@t2u.com");

        providerUserAccount.setAccount(accountUser);

        List<IBean> lstAccount = dsrc.Select(providerUserAccount);

        json = jsonBuilder.serialize(lstAccount);

        System.out.println( "RESULT:" + json);

        assertFalse(json.isEmpty());
//        assertEquals(true, true);
    }
}

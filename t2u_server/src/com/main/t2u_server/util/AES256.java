package com.main.t2u_server.util;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import java.security.AlgorithmParameters;
import org.apache.commons.codec.binary.Base64;

/**
 * Created by DEVELOPPEUR on 12/08/2015.
 */
public class AES256 {

    private static int pswdIterations = 65536;
    private static int keySize = 256;

    public AESMessage encrypt(String clearValue, String key, String salt) {
        try {
            /* Derive the key, given password and salt. */
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            PBEKeySpec spec = new PBEKeySpec(
                    key.toCharArray(),
                    salt.getBytes("UTF-8"),
                    pswdIterations,
                    keySize
            );

            SecretKey tmp = factory.generateSecret(spec);
            SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");

            /* Encrypt the message. */
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secret);
            AlgorithmParameters params = cipher.getParameters();
            byte[] ivBytes = params.getParameterSpec(IvParameterSpec.class).getIV();
            byte[] ciphertext = cipher.doFinal(clearValue.getBytes("UTF-8"));

            return new AESMessage(new Base64().encodeAsString(ciphertext), ivBytes);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }

    public String decrypt(AESMessage encryptValue, String key, String salt) {
        try {
            byte[] saltBytes = salt.getBytes("UTF-8");
            byte[] encryptedTextBytes = new Base64().decodeBase64(encryptValue.getCiphertext());
            byte[] decryptedTextBytes = null;

            // Derive the key
            SecretKeyFactory factory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
            PBEKeySpec spec = new PBEKeySpec(
                    key.toCharArray(),
                    saltBytes,
                    pswdIterations,
                    keySize
            );

            SecretKey tmp = factory.generateSecret(spec);
            SecretKey secret = new SecretKeySpec(tmp.getEncoded(), "AES");

            // Decrypt the message
            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(encryptValue.getIvBytes()));

            decryptedTextBytes = cipher.doFinal(encryptedTextBytes);

            return new String(decryptedTextBytes);
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
            return null;
        } catch (BadPaddingException e) {
            e.printStackTrace();
            return null;
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            return null;
        }
    }
}
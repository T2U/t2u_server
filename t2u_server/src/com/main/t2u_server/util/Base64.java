package com.main.t2u_server.util;

import javax.xml.bind.DatatypeConverter;

/**
 * Created by DEVELOPPEUR on 23/06/2015.
 * Src <>http://stackoverflow.com/questions/14413169/which-java-library-provides-base64-encoding-decoding</>
 */
public class Base64 {

    /**
     * Basic Java 6 * 7 Compatible Base64 encoder
     */
    public static String  Encode( byte[] message)
    {
       // byte[] message = "hello world".getBytes("UTF-8");
        return  DatatypeConverter.printBase64Binary(message);
    }

    public static byte[] Decode(String encoded)
    {
        return DatatypeConverter.parseBase64Binary(encoded);
    }

/*
 Java 8

Java 8 now provides an additional utility class for this java.util.Base64

byte[] message = "hello world".getBytes(StandardCharsets.UTF_8);
String encoded = Base64.getEncoder().encodeToString(message);
byte[] decoded = Base64.getDecoder().decode(encoded);

System.out.println(encoded);
System.out.println(new String(decoded, StandardCharsets.UTF_8));
Output

aGVsbG8gd29ybGQ=
hello world
Java 6 and 7

Since Java 6 you can use the lesser known class javax.xml.bind.DatatypeConverter. This is part of the JRE, no extra libraries required.

byte[] message = "hello world".getBytes("UTF-8");
String encoded = DatatypeConverter.printBase64Binary(message);
byte[] decoded = DatatypeConverter.parseBase64Binary(encoded);

System.out.println(encoded);
System.out.println(new String(decoded, "UTF-8"));
Output

aGVsbG8gd29ybGQ=
hello world
* */
}

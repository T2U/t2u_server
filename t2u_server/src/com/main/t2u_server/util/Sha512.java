package com.main.t2u_server.util;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import org.apache.commons.lang3.CharEncoding;

/**
 * Created by DEVELOPPEUR on 06/05/2015.
 */
public class Sha512 {

    // org.apache.poi.poifs.crypt.HashAlgorithm
    public static final String sha512 = "SHA-512";

    public static String getSha512(String inputValue)
    {
        return getSha512(inputValue, "");
    }

    public static String getSha512(String inputValue, String salt)
    {
        String result = "";

        try {
            MessageDigest digest = MessageDigest.getInstance(sha512);
            digest.reset();
            digest.update(salt.getBytes());

            byte[] hash = digest.digest(inputValue.getBytes(CharEncoding.UTF_8));

            result = org.apache.commons.codec.binary.Hex.encodeHexString(hash);
        }
        catch (NoSuchAlgorithmException ex) {
            System.out.println(ex.getMessage());
        }
        finally {
            return  result;
        }
    }
}

package com.main.t2u_server.util;

/**
 * Created by DEVELOPPEUR on 06/06/2015.
 */
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

public class HibernateUtil
{
    private static SessionFactory sessionFactory = buildSessionFactory();

    private static SessionFactory buildSessionFactory()
    {
        try
        {
            if (sessionFactory == null)
            {
                Configuration configuration = new Configuration().configure(HibernateUtil.class.getResource("/hibernate.cfg.xml"));

                String OPENSHIFT_WILDFLY_IP = System.getenv("OPENSHIFT_WILDFLY_IP");

                System.out.println("#alexis# OPENSHIFT_WILDFLY_IP=" + OPENSHIFT_WILDFLY_IP);

                if(OPENSHIFT_WILDFLY_IP != null && !OPENSHIFT_WILDFLY_IP.isEmpty())
                {
                    configuration.setProperty("hibernate.connection.url", "jdbc:h2:~/app-root/repo/db/T2UH2");
                }

                StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
                serviceRegistryBuilder.applySettings(configuration.getProperties());
                ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
                sessionFactory = configuration.buildSessionFactory(serviceRegistry);
            }
            return sessionFactory;
        } catch (Throwable ex)
        {
            System.err.println("Initial SessionFactory creation failed." + ex);
            throw new ExceptionInInitializerError(ex);
        }
    }

    public static SessionFactory getSessionFactory()
    {
        return sessionFactory;
    }

    public static void shutdown()
    {
        getSessionFactory().close();
    }
}
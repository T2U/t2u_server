package com.main.t2u_server.util;

/**
 * Created by DEVELOPPEUR on 12/08/2015.
 */
public class AESMessage {
    protected byte[] ivBytes;
    public byte[] getIvBytes() {return ivBytes;}

    public void setCiphertext(String ciphertext) {
        this.ciphertext = ciphertext;
    }

    public void setIvBytes(byte[] ivBytes) {
        this.ivBytes = ivBytes;
    }

    protected String ciphertext;
    public String getCiphertext() {return ciphertext;}

    public AESMessage(){}

    public AESMessage(String ciphertext, byte[] ivBytes){
        this.ivBytes = ivBytes;
        this.ciphertext = ciphertext;
    }
}

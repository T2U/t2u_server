package com.main.t2u_server.service;

import com.main.t2u_server.bean.Token;
import com.main.t2u_server.dataSource.GroupDataSource;
import com.main.t2u_server.dataSource.IDataSource;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.util.Series;

/**
 * Created by DEVELOPPEUR on 10/06/2015.
 */
public class ServiceGroup extends AServiceT2U {
    // Cr�ation d'une dataSource pour l'insertion dun nouveau compte
    IDataSource dsrcGroup;

    public ServiceGroup() {
        super();

        this.authService  = new ServiceAuthentication();
        this.dsrcGroup = new GroupDataSource();
    }

    /**
     * Retourne la liste des Groupes dans la base de donn�es.
     *
     * @param entity
     * @return
     */
    @Get
    public Representation getLstGroup(Representation entity) {
//        ServiceAuthentication authService = new ServiceAuthentication();
        Token userToken = null;

        // R�cup�ration des possibles param�tres
        try {

            //System.out.println(entity.getText());

            Series headers = (Series) getRequestAttributes().get("org.restlet.http.headers");
            String auth = headers.getFirstValue("Token");

            userToken = jsonBuilder.deserialize(auth, Token.class);

        } catch (Exception ex) {
            System.out.println("#tag01#" + ex.getMessage());
            userToken = new Token();
        }

        if(!authService.checkTokenValidity(userToken))
            throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED, "Invalid Token Connection refused", null);

       return  getLstIBean(entity, dsrcGroup);
    }
}

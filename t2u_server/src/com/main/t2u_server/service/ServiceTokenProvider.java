package com.main.t2u_server.service;

import com.main.t2u_server.bean.Account;
import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.bean.Token;
import com.main.t2u_server.dataSource.ADataSource;
import com.main.t2u_server.dataSource.AccountDataSource;
import com.main.t2u_server.dataSource.IDataSource;
import com.main.t2u_server.dataSource.TokenDataSource;
import com.main.t2u_server.util.Sha256;
import com.owlike.genson.Genson;
import org.restlet.data.CharacterSet;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;

import java.util.List;

/**
 * Created by DEVELOPPEUR on 08/05/2015.
 */
public class ServiceTokenProvider extends AServiceT2U {
    IDataSource dsrcToken;
    IDataSource dsrcAccount;

    public ServiceTokenProvider() {
        super();

        dsrcAccount = new AccountDataSource();
        dsrcToken = new TokenDataSource();
        jsonBuilder = new Genson();
    }

    /**
     * Authentification de l'utilisateur et r�cup�ration d'un token.
     * @param entity
     * @return
     */
    @Post
    public Representation postAccountForLogin(Representation entity) {
        Account compteSaisie;
        Account compteEnBase;
        Representation representation;
        String jsonObject = null;

        try {
            compteSaisie = jsonBuilder.deserialize(entity.getText(), Account.class);

            // Check Password and Return Token
            List<IBean> lstAccount = dsrcAccount.Select(compteSaisie);

            if(lstAccount != null && lstAccount.size() == 1) {
                compteEnBase = (Account) lstAccount.get(lstAccount.size() - 1);

                // Comparaison de Mot de passe
                if (!compteEnBase.getPassword().equals(Sha256.getSha256(compteSaisie.getPassword())))
                    throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED, "Connection refused", null);

                // Retourner un token,
                Token objet = new Token(compteEnBase.getId_account());

                jsonObject = jsonBuilder.serialize(dsrcToken.Insert(objet));

                representation = new StringRepresentation(jsonObject, MediaType.TEXT_PLAIN);
            }
            else {
                throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED, "Connection refused", null);
            }

        } catch (ADataSource.ADataSourceException ex) {
            //System.out.println(ex.getMessage());
            throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED, "Connection refused : " + ex.getMessage(), ex);
        } catch (java.io.IOException ex) {
            //System.out.println(ex.getMessage());
            throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED, "Connection refused", ex);
        }

        representation = new StringRepresentation(jsonObject, MediaType.TEXT_PLAIN);

        representation.setCharacterSet(CharacterSet.UTF_8);

        return representation;
    }

    /**
     * Donne un Token � un utilisateur ou une API.
     * @return
     */
    @Get
    public Representation getToken(Representation entity) {
        return  getLstIBean(entity, dsrcToken);
    }

//    @Delete
//    public Representation deleteToken(Representation entity) {
//        String jsonObject;
//        Representation representation;
//
//        try {
//            jsonObject = jsonBuilder.serialize(dsrcToken.Select(0));
//        } catch (java.sql.SQLException ex) {
//            System.out.println(ex.getMessage());
//            throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED, "ERROR when adding", ex);
//        }
//
//        representation = new StringRepresentation(jsonObject, MediaType.TEXT_PLAIN);
//        representation.setCharacterSet(CharacterSet.UTF_8);
//
//        return representation;
//    }
}

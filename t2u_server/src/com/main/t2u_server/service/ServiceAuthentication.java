package com.main.t2u_server.service;

import com.main.t2u_server.bean.Account;
import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.bean.Token;
import com.main.t2u_server.dataSource.AccountDataSource;
import com.main.t2u_server.dataSource.IDataSource;
import com.main.t2u_server.dataSource.TokenDataSource;
import com.main.t2u_server.util.Sha256;
import org.joda.time.DateTime;

import java.util.List;

/**
 * Cette classe permet de donn�er les droits d'acc�s au information.
 * Created by DEVELOPPEUR on 20/06/2015.
 */
public class ServiceAuthentication extends AServiceT2U {
    // Cr�ation d'une dataSource pour l'insertion dun nouveau compte
    IDataSource dsrcAccount;
    IDataSource dsrcToken;

    public ServiceAuthentication() {
        super();

        dsrcAccount = new AccountDataSource();
        dsrcToken = new TokenDataSource();
    }

    /**
     * V�rifier la validit� d'un Account
     *
     * @param compteSaisie
     * @return
     */
    public Boolean checkAccount(Account compteSaisie) {
        Account compteEnBase;
        Boolean accountValidity = Boolean.FALSE;

        try {
            List<IBean> lstAccount = dsrcAccount.Select(compteSaisie);

            if (lstAccount != null && lstAccount.size() == 1) {
                compteEnBase = (Account) lstAccount.get(lstAccount.size() - 1);

                // Comparaison de Mot de passe
                if (!compteEnBase.getPassword().equals(Sha256.getSha256(compteSaisie.getPassword())))
                    accountValidity = Boolean.FALSE;
                else
                    accountValidity = Boolean.TRUE;
            }
        } catch (Exception ex) {
            accountValidity = Boolean.FALSE;
        } finally {
            return accountValidity;
        }
    }

    /**
     * V�rifier la validit� d'un Token. Si exite mais plus valide on le supprime de la table.
     *
     * @param tokenSaisie
     * @return
     */
    public Boolean checkTokenValidity(Token tokenSaisie) {
        Token tokenEnBase;
        Boolean tokenValidity = Boolean.FALSE;

        try {
            List<IBean> lstToken = dsrcToken.Select(tokenSaisie);

            if (lstToken != null && lstToken.size() == 1) {
                tokenEnBase = (Token) lstToken.get(lstToken.size() - 1);

                // Comparaison de la valeur Data
                if (tokenEnBase.getData().equals(tokenSaisie.getData())) {

                    DateTime _currentUtc = DateTime.now();
                    DateTime _expiresUtc = DateTime.parse(tokenEnBase.getExpiresUtc());

                    if (_expiresUtc.getMillis() > _currentUtc.getMillis())
                        tokenValidity = Boolean.TRUE;
                    else {

                        tokenValidity = Boolean.FALSE;

                        // Suppression du token plus valide.
                        dsrcToken.Delete(tokenEnBase);
                    }
                } else
                    tokenValidity = Boolean.FALSE;
            }
        } catch (Exception ex) {
            tokenValidity = Boolean.FALSE;
        } finally {
            return tokenValidity;
        }
    }

    /**
     * D�termine si le token � le droit d'acc�ss Read, Write, RWX sur le service cible.
     *
     * @param tokenSaisie
     * @param serviceTarget
     * @return
     */
    public Boolean checkRightAccess(Token tokenSaisie, AServiceT2U serviceTarget) {
        Boolean checkRightAccess = Boolean.FALSE;

        if (checkTokenValidity(tokenSaisie)) {
            Account account;
            try {
                for (IBean iBean : dsrcToken.Select(tokenSaisie)) {
                    Token token = (Token) iBean;

                    for (IBean bean : dsrcAccount.Select(token.getAccount())) {

                        break;
                    }
                    break;
                }
            }catch (Exception ex) {
                checkRightAccess = Boolean.FALSE;
            }


            checkRightAccess = Boolean.TRUE;
        } else {
            checkRightAccess = Boolean.FALSE;
        }

        return  checkRightAccess;
    }

    public enum AccesMode{
        READ,
        WRITE,
        READWRITE,
        FULL
    }
}

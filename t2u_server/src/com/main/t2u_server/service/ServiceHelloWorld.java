package com.main.t2u_server.service;

import com.main.t2u_server.bean.Account;
import com.main.t2u_server.bean.Group;
import com.owlike.genson.Genson;

import com.owlike.genson.GensonBuilder;
import org.restlet.data.CharacterSet;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Etudiant on 11/02/2015.
 */
public class ServiceHelloWorld extends ServerResource {

    @Get
    public Representation handleGetGroupe() {
        List<Account> lst = new ArrayList<Account>();

        String acountName = "toto#01234";
        lst.add(new Account(1,acountName, new Group(acountName),"","Groupe ISITECH"));

        acountName = "toto#01235";
        lst.add(new Account(1,acountName, new Group(acountName),"","Groupe ISITECH"));

        acountName = "toto#01236";
        lst.add(new Account(1,acountName, new Group(acountName),"","Groupe ISITECH"));

        Genson genson = new GensonBuilder().useClassMetadata(true).create();

        String json;
        try {
            json = genson.serialize(lst);
        }
        catch (Exception ex) {
            json = "{" + ex.getMessage() + "}";
        }

       // return obj.toJson(lst);
        //return json;
        
        Representation representation = new StringRepresentation(json, MediaType.TEXT_PLAIN);
        representation.setCharacterSet(CharacterSet.UTF_8);
        return representation;
    }
}

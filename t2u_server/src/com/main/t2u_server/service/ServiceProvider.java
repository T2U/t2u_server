package com.main.t2u_server.service;

import com.main.t2u_server.dataSource.IDataSource;
import com.main.t2u_server.dataSource.ProviderDataSource;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;

/**
 * Created by DEVELOPPEUR on 20/06/2015.
 */
public class ServiceProvider   extends AServiceT2U  {
    IDataSource dsrcProvider;

    public ServiceProvider() {
        super();

        dsrcProvider = new ProviderDataSource();
    }

    @Get
    public Representation getLstProvider(Representation entity) {
       return this.getLstIBean(entity, dsrcProvider);
    }
}

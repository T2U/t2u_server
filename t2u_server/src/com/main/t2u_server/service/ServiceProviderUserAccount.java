package com.main.t2u_server.service;

import com.main.t2u_server.bean.*;
import com.main.t2u_server.dataSource.*;
import com.main.t2u_server.util.AES256;
import com.main.t2u_server.util.AESMessage;
import com.main.t2u_server.util.Sha256;
import com.main.t2u_server.util.Sha512;
import org.restlet.data.CharacterSet;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Delete;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;

import java.util.List;

/**
 * Created by DEVELOPPEUR on 20/06/2015.
 */
public class ServiceProviderUserAccount  extends AServiceT2U  {
    IDataSource dsrcProviderUserAccount;
    IDataSource dsrcAccount;
    IDataSource dsrcProvider;

    public ServiceProviderUserAccount() {
        super();

        dsrcProviderUserAccount = new ProviderUserAccountDataSource();
        dsrcAccount = new AccountDataSource();
        dsrcProvider = new ProviderDataSource();
    }

    @Get
    public Representation getLstProviderUserAccount(Representation entity) {
        String json;
        Representation representation;
        List<IBean> listProviderUserAccount = null;
        ProviderUserAccount leCompte = null;

        String jsonObject = null;
        System.out.println("getLstProviderUserAccount");

        // R�cup�ration des possibles param�tres
        try {
            //System.out.println("entity : " + entity.getText());
            jsonObject = getQueryValue("criteria");

            leCompte = jsonBuilder.deserialize(jsonObject, ProviderUserAccount.class);

            //id = Integer.valueOf(getQuery().getValues("id"));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            jsonObject = null;
            leCompte = null;
        }

        try {
            listProviderUserAccount = dsrcProviderUserAccount.Select(leCompte);

            json = jsonBuilder.serialize(listProviderUserAccount);

        } catch (Exception ex) {
            json = "{" + ex.getMessage() + "}";
        }

        representation = new StringRepresentation(json, MediaType.TEXT_PLAIN);
        representation.setCharacterSet(CharacterSet.UTF_8);

        return representation;

        //return this.getLstIBean(entity, dsrcProviderUserAccount);
    }

    /**
     * Cr�er un nouveau Compte de fournisseur dans la base de donn�es
     *
     * @param entity
     * @return
     */
    @Post
    public Representation postNewProviderUserAccount(Representation entity) {
        ProviderUserAccount nouveauCompte;

        Account leCompte;
        Provider leFournisseur;
        Representation representation = null;

        ServiceAuthentication serviceAuthentication;
        AES256 encryptor;
        Account clearAccount = null;

        List<IBean> lstStamp;

        String jsonObject = null;

        try {
            jsonObject = entity.getText();

            nouveauCompte = jsonBuilder.deserialize(jsonObject, ProviderUserAccount.class);

            // Récupération du password en clair
            clearAccount = nouveauCompte.getAccount();

            // Récupération du compte.
            lstStamp = dsrcAccount.Select(nouveauCompte.getAccount());

            if(lstStamp.size() == 1) {
                leCompte = (Account)lstStamp.get(0);
                System.out.println(leCompte);

                // Récupération du fournisseur.
                lstStamp = dsrcProvider.Select(nouveauCompte.getProvider());

                if(lstStamp.size() == 1) {
                    leFournisseur = (Provider)lstStamp.get(0);

                    System.out.println(leFournisseur);

                    nouveauCompte.setAccount(leCompte);
                    nouveauCompte.setProvider(leFournisseur);

                    // Si le nouveauCompte à un username et un password
                    if(!nouveauCompte.getUsername().isEmpty() &&
                            !nouveauCompte.getPassword().isEmpty()) {

                        System.out.println(nouveauCompte);

                        serviceAuthentication = new ServiceAuthentication();

                        // Is the real password of account
                        if(serviceAuthentication.checkAccount(clearAccount)){
                            System.out.println("PASSWORD CHECK DONE");

                            encryptor = new AES256();

                            nouveauCompte.getAccount().setPassword(clearAccount.getPassword());
                            nouveauCompte.setPasswordv2(encryptPassowrd(nouveauCompte));
                            nouveauCompte.setUsernamev2(encryptLogin(nouveauCompte));
                            nouveauCompte.setUsername("$PROTECTED$");

                            jsonObject = jsonBuilder.serialize(dsrcProviderUserAccount.Insert(nouveauCompte));

                            representation = new StringRepresentation(jsonObject, MediaType.TEXT_PLAIN);

                            // Log
                            AccountLog anAccountLog = new AccountLog(0, nouveauCompte.getAccount(), AccountLog.TypeLog.INFORMATION , "Successfully add new account for " + nouveauCompte.getProvider().getName() + " provider");
                            AccountLogDataSource otemdsrc = new AccountLogDataSource();
                            otemdsrc.Insert(anAccountLog);
                        }
                        else{
                            System.out.println("PASSWORD CHECK BAD");
                            throw new ResourceException(Status.CLIENT_ERROR_NOT_ACCEPTABLE, "Invalid main pawword");
                        }
                    }
                    else {
                        throw new ResourceException(Status.CLIENT_ERROR_NOT_ACCEPTABLE, "Username or password are not acceptable");
                    }
                }
                else {
                    throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, "Provider[" + nouveauCompte.getProvider().getName() + "]not found");
                }
            }
            else {
                throw new ResourceException(Status.CLIENT_ERROR_NOT_FOUND, "Account[" + nouveauCompte.getAccount().getEmail() + "]not found");
            }
        } catch (ADataSource.ADataSourceException ex) {
            System.out.println(ex.getMessage());
            throw new ResourceException(Status.CLIENT_ERROR_CONFLICT, "ADataSourceException[" + ex.getMessage() + "]", ex);
        } catch (java.io.IOException ex) {
            System.out.println(ex.getMessage());
            throw new ResourceException(Status.CLIENT_ERROR_CONFLICT, "IOException[" + ex.getMessage() + "]", ex);
        }

        if(representation == null)
            representation = new StringRepresentation("{}", MediaType.TEXT_PLAIN);

        representation.setCharacterSet(CharacterSet.UTF_8);

        return representation;
    }

    /**
     * Supprimer un Compte de fournisseur dans la base de donn�es
     *
     * @param entity
     * @return
     */
    @Delete
    public Representation deleteProviderUserAccount(Representation entity) {
        String json;
        Representation representation;
        List<IBean> listProviderUserAccount = null;
        ProviderUserAccount leCompte = null;

        String jsonObject = null;

        // R�cup�ration des possibles param�tres
        try {
            //System.out.println("entity : " + entity.getText());
            jsonObject = getQueryValue("criteria");

            leCompte = jsonBuilder.deserialize(jsonObject, ProviderUserAccount.class);

            //id = Integer.valueOf(getQuery().getValues("id"));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            jsonObject = null;
            leCompte = null;
        }

        try {
            listProviderUserAccount = dsrcProviderUserAccount.Select(leCompte);

            leCompte = (ProviderUserAccount)listProviderUserAccount.get(0);

            listProviderUserAccount = dsrcProviderUserAccount.Delete(leCompte);

            json = jsonBuilder.serialize(listProviderUserAccount);


            // Log
            AccountLog anAccountLog = new AccountLog(0, leCompte.getAccount(), AccountLog.TypeLog.INFORMATION , "Successfully delete account for " + leCompte.getProvider().getName() + " provider");
            AccountLogDataSource otemdsrc = new AccountLogDataSource();
            otemdsrc.Insert(anAccountLog);

        } catch (Exception ex) {
            json = "{" + ex.getMessage() + "}";
        }

        representation = new StringRepresentation(json, MediaType.TEXT_PLAIN);
        representation.setCharacterSet(CharacterSet.UTF_8);

        return representation;
    }

    /**
     * Encryption et decryption du password
     */
    private static String salt = "8QGV1TTASOSZATSLAGRY3HBSALETSPY2EZ22VBGABWZGK34GCM6330XCNYONKJPA4VYOG01SYJPN17Y4YA2Y8F3PELOZJZMNXCD59J85E4N8YBCPQYJPMXDBBLNFMH2P";

    /**
     * Retourne le password du ProviderUserAccount encrypté avec le mot de passe en clair du compte associé
     */
    public static AESMessage encryptPassowrd(ProviderUserAccount providerUserAccount){
        AES256 cryptor = new AES256();

        return cryptor.encrypt(providerUserAccount.getPassword(), Sha512.getSha512(providerUserAccount.getAccount().getPassword(), salt), salt);
    }

    public static AESMessage encryptLogin(ProviderUserAccount providerUserAccount){
        AES256 cryptor = new AES256();

        return cryptor.encrypt(providerUserAccount.getUsername(), Sha512.getSha512(providerUserAccount.getAccount().getPassword(), salt), salt);
    }

    /**
     * Retourne le password en clair du ProviderUserAccount décrypté avec le mot de passe en clair du compte associé.
     */
    public static String decryptPassord(ProviderUserAccount providerUserAccount){
        AES256 cryptor = new AES256();

        return cryptor.decrypt(providerUserAccount.getPasswordv2(), Sha512.getSha512(providerUserAccount.getAccount().getPassword(), salt), salt);
    }

    public static String decryptLogin(ProviderUserAccount providerUserAccount){
        AES256 cryptor = new AES256();

        return cryptor.decrypt(providerUserAccount.getUsernamev2(), Sha512.getSha512(providerUserAccount.getAccount().getPassword(), salt), salt);
    }
}

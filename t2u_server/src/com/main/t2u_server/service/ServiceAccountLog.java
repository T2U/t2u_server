package com.main.t2u_server.service;

import com.main.t2u_server.bean.AccountLog;
import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.dataSource.AccountDataSource;
import com.main.t2u_server.dataSource.AccountLogDataSource;
import com.main.t2u_server.dataSource.IDataSource;
import org.restlet.data.CharacterSet;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;

import java.util.List;

/**
 * Created by DEVELOPPEUR on 08/08/2015.
 */
public class ServiceAccountLog  extends AServiceT2U  {
    IDataSource dsrcAccountLog;
    IDataSource dsrcAccount;

    public ServiceAccountLog() {
        super();

        dsrcAccountLog = new AccountLogDataSource();
        dsrcAccount = new AccountDataSource();
    }

    /**
     * Récupération d'une liste de AccountLog depuis un Crtitère de selection sur AccountLog
     * @param entity
     * @return
     */
    @Get
    public Representation getLstAccountLog(Representation entity) {
        String json;
        Representation representation;
        List<IBean> listAccountLog = null;
        AccountLog leCritereLog = null;

        String jsonObject = null;

        // Récupération des possibles param�tres
        try {
            jsonObject = getQueryValue("criteria");

            leCritereLog = jsonBuilder.deserialize(jsonObject, AccountLog.class);
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            jsonObject = null;
            leCritereLog = null;
        }

        try {
            listAccountLog = dsrcAccountLog.Select(leCritereLog);

            json = jsonBuilder.serialize(listAccountLog);

        } catch (Exception ex) {
            json = "{" + ex.getMessage() + "}";
        }

        representation = new StringRepresentation(json, MediaType.TEXT_PLAIN);
        representation.setCharacterSet(CharacterSet.UTF_8);

        return representation;
    }
}

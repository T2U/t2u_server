package com.main.t2u_server.service;

import com.main.t2u_server.bean.Account;
import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.dataSource.ADataSource;
import com.main.t2u_server.dataSource.AccountDataSource;
import com.main.t2u_server.dataSource.IDataSource;
import com.main.t2u_server.util.Sha256;

import org.restlet.data.*;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ResourceException;

/**
 * Service pour la gestion des Comptes utilisateurs / Account
 * <p/>
 * Created by DEVELOPPEUR on 02/05/2015.
 */
public class SerivceAccount extends AServiceT2U {

    // Cr�ation d'une dataSource pour l'insertion dun nouveau compte
    IDataSource dsrcAccount;

    public SerivceAccount() {
        super();

        dsrcAccount = new AccountDataSource();
    }

    /**
     * Retourne la liste des Accounts dans la base de donn�es.
     *
     * @param entity
     * @return
     */
    @Get
    public Representation getLstAccount(Representation entity) {
        return getLstIBean(entity, dsrcAccount);
    }

    /**
     * Cr�er un nouvel Account dans la base de donn�es
     *
     * @param entity
     * @return
     */
    @Post("/account/register")
    public Representation postNewAccount(Representation entity) {
        Account nouveauCompte;
        Representation representation;

        String jsonObject = null;

        try {
            jsonObject = entity.getText();

            nouveauCompte = jsonBuilder.deserialize(jsonObject, Account.class);

            // Encoder le Mot de passe
            nouveauCompte.setPassword(encodePassword(nouveauCompte));

            /* Si l'utilisateur n'a pas indiqu� de Uuid on g�n�re un nouveau Uuid */
            if (nouveauCompte.getUuid() == null || nouveauCompte.getUuid() == 0) {
                String query = "FROM Account E WHERE E.uuid > 500 ORDER BY E.uuid DESC";

                for (IBean iBean : dsrcAccount.Select(query)) {
                    Account lastAccount = (Account) iBean;

                    nouveauCompte.setUuid(lastAccount.getUuid() + 1);
                    break;
                }
            }

            jsonObject = jsonBuilder.serialize(dsrcAccount.Insert(nouveauCompte));

            representation = new StringRepresentation(jsonObject, MediaType.TEXT_PLAIN);
        } catch (ADataSource.ADataSourceException ex) {
            System.out.println(ex.getMessage());
            throw new ResourceException(Status.CLIENT_ERROR_CONFLICT, "ADataSourceException[" + ex.getMessage() + "]", ex);
        } catch (java.io.IOException ex) {
            System.out.println(ex.getMessage());
            throw new ResourceException(Status.CLIENT_ERROR_CONFLICT, "IOException[" + ex.getMessage() + "]", ex);
        }

        representation.setCharacterSet(CharacterSet.UTF_8);

        return representation;
    }

    /**
     * Méthode d'encodage du mots de passe de l'utilisateur
     * @param account
     * @return
     */
    public static String encodePassword(Account account){
        return Sha256.getSha256(account.getPassword());
    }
}

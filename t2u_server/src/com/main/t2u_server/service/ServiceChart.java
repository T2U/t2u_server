package com.main.t2u_server.service;

import com.main.t2u_server.bean.Chart;
import com.main.t2u_server.bean.EnteteDocument;
import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.bean.ProviderUserAccount;
import com.main.t2u_server.dataSource.AccountDataSource;
import com.main.t2u_server.dataSource.ChartDataSource;
import com.main.t2u_server.dataSource.IDataSource;
import org.restlet.data.CharacterSet;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;

import java.util.List;

/**
 * Created by DEVELOPPEUR on 22/08/2015.
 */
public class ServiceChart extends AServiceT2U {

    // Cr�ation d'une dataSource pour l'insertion dun nouveau compte
    IDataSource dsrcChart;

    public ServiceChart() {
        super();

        dsrcChart = new ChartDataSource();
    }

    /**
     * Récupération d'une Liste de Charts/Graphiques
     * @param entity
     * @return
     */
    @Get
    public Representation getLstChart(Representation entity) {
        String json;
        Representation representation;
        List<IBean> listChart = null;
        Chart leChart = null;

        // Vérification du Token
        if(!checkToken()) {
            throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED, "Invalid Token Connection refused", null);
        }
        else {

            // R�cup�ration des possibles param�tres
            try {
                json = getQueryValue("criteria");

                leChart = jsonBuilder.deserialize(json, Chart.class);
            } catch (Exception ex) {
                System.out.println(ex.getMessage());
                leChart = null;
            }

            // Récupération des Charts
            try {
                listChart = dsrcChart.Select(leChart);



                for(IBean object : listChart)
                {
                    Chart oChart = (Chart)object;

                    for(IBean object02 : oChart.getEnteteDocumentList())
                    {
                        EnteteDocument oEnteteDocument = (EnteteDocument)object02;

                        oEnteteDocument.getProviderUserAccount().getAccount().setGroupList(null);
                    }
                }

                json = jsonBuilder.serialize(listChart);

            } catch (Exception ex) {
                json = "{" + ex.getMessage() + "}";
            }
        }

        representation = new StringRepresentation(json, MediaType.TEXT_PLAIN);
        representation.setCharacterSet(CharacterSet.UTF_8);

        return representation;
    }
}

package com.main.t2u_server.service;

import com.main.t2u_server.bean.Facture;
import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.dataSource.ADataSource;
import com.main.t2u_server.dataSource.FactureDataSource;
import com.main.t2u_server.dataSource.IDataSource;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;

import java.util.List;

@Deprecated
public class ServiceFacture extends AServiceT2U {
    public static String JOURFACTUREFREE = "02";

    IDataSource dsrcFacture;

    public ServiceFacture () {
        super();

        dsrcFacture = new FactureDataSource();
    }
    /**
     * Retourne la liste des Factures de la base de données.
     *
     * @param entity
     * @return
     */
    @Get
    public Representation getLstAccount(Representation entity) {
        return  getLstIBean(entity, dsrcFacture);
    }

    /**
     * Doit disparaitre
     */
    public Facture creerFacture(String date, String montant, String fichier) throws java.sql.SQLException {
        String arr[] = montant.split(" ", 2);

        Facture facFree = new Facture();

        facFree.setDate(conversionDate(date));
        facFree.setMontant(Double.parseDouble(arr[0]));

        try {

            List<IBean> iBeanList = dsrcFacture.Insert(facFree);

           return (Facture)iBeanList.get(0);
        }
        catch (ADataSource.ADataSourceException ex)
        {
            return null;
        }
    }

    /**
     * Convertisseur Date
     * @param date
     * @return
     */
    public String conversionDate(String date){
        String arr[] = date.split(" ", 2);
        String mois="";
        String annee="";
        for(int i=0;i<arr.length;i++){
            System.out.println(arr[i]);
			if(i == 0){
                mois = arr[0];
            }else if(i == 1) {
                annee = arr[1];
            }
        }

        switch(mois.toLowerCase()){
            case "janvier": mois = "01"; break;
            case "février": mois = "02"; break;
            case "mars": mois = "03"; break;
            case "avril": mois = "04"; break;
            case "mai": mois = "05"; break;
            case "juin": mois = "06"; break;
            case "juillet": mois = "07"; break;
            case "août": mois = "08"; break;
            case "septembre": mois = "09"; break;
            case "octobre": mois = "10"; break;
            case "novembre": mois = "11"; break;
            case "décembre": mois = "12"; break;
			default:mois= "00"; break;
        }

        return JOURFACTUREFREE+"/"+mois+"/"+annee;
    }
}

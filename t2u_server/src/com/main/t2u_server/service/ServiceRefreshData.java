package com.main.t2u_server.service;

import com.main.t2u_server.bean.Account;
import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.bean.ProviderUserAccount;
import com.main.t2u_server.bean.Token;
import com.main.t2u_server.dataSource.ADataSource;
import com.main.t2u_server.dataSource.IDataSource;
import com.main.t2u_server.dataSource.ProviderUserAccountDataSource;
import com.main.t2u_server.dataSource.RefreshDataDataSource;
import com.main.t2u_server.util.AES256;
import org.restlet.data.CharacterSet;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ResourceException;
import org.restlet.util.Series;

import java.util.List;

/**
 * Created by DEVELOPPEUR on 13/08/2015.
 */
public class ServiceRefreshData extends AServiceT2U  {
    IDataSource dsrcProviderUserAccount;
    IDataSource dsrcRefreshDataDataSource;

    public ServiceRefreshData() {
        super();

        dsrcProviderUserAccount = new ProviderUserAccountDataSource();
        dsrcRefreshDataDataSource = new RefreshDataDataSource();
    }

    /**
     * Rafaichire les données d'un compte de fournisseur.
     * @param entity
     * @return
     */
    @Get
    public Representation getRefreshData(Representation entity) {
        Representation representation;
        ProviderUserAccount leCritere = null;
        String jsonObject = null;
        Token userToken = null;

        Account clearAccount = null;
        ProviderUserAccount leCompteFournisseur;

        ServiceAuthentication authService = new ServiceAuthentication();

        // R�cup�ration du Token
        try {
            Series headers = (Series) getRequestAttributes().get("org.restlet.http.headers");

            userToken = jsonBuilder.deserialize(headers.getFirstValue("Token"), Token.class);
        } catch (Exception ex) {
            throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED, "Invalid Token Connection refused", null);
        }

        // Vérification du Token
        if(!authService.checkTokenValidity(userToken)) {
            throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED, "Invalid Token Connection refused", null);
        }
        else {
            // Récupération du ProviderUserAccount
            try {
                jsonObject = getQueryValue("criteria");

                leCritere = jsonBuilder.deserialize(jsonObject, ProviderUserAccount.class);

                // Récupération du Compte Principal
                clearAccount = leCritere.getAccount();

                // Vérification du Mot de passe Principal
                if(authService.checkAccount(clearAccount)){

                    // Récupération du Compte de fournisseur de service
                    List<IBean> listProviderUserAccount = dsrcProviderUserAccount.Select(leCritere);

                    // If Provider not found
                    if(listProviderUserAccount.isEmpty()){
                        throw new ResourceException(Status.CLIENT_ERROR_NOT_ACCEPTABLE, "Invalid unknown ProviderUserAccount");
                    }

                    // Récupération du Compte de Fournisseur
                    leCompteFournisseur = (ProviderUserAccount)listProviderUserAccount.get(0);

                    // Set Clear Password
                    leCompteFournisseur.getAccount().setPassword(clearAccount.getPassword());
                    leCompteFournisseur.setPassword(ServiceProviderUserAccount.decryptPassord(leCompteFournisseur));

                    if(leCompteFournisseur.getUsername().equals("$PROTECTED$")) {
                        // Set Clear UserName
                        leCompteFournisseur.setUsername(ServiceProviderUserAccount.decryptLogin(leCompteFournisseur));
                    }

                    // TODO REFRESH DATA
                    listProviderUserAccount.clear();

                    // Effacer le mot de passe en clair
                    clearAccount.setPassword("$PROTECTED$");
                    leCompteFournisseur.getAccount().setPassword("$PROTECTED$");

                    /* Rafraichir et retourner les informations du compte de service */
                    listProviderUserAccount = dsrcRefreshDataDataSource.Select(leCompteFournisseur);

                    jsonObject = jsonBuilder.serialize(listProviderUserAccount);
                }
                else{
                    throw new ResourceException(Status.CLIENT_ERROR_NOT_ACCEPTABLE, "Invalid main password");
                }
            } catch (ADataSource.ADataSourceException ex) {
                throw new ResourceException(Status.CLIENT_ERROR_BAD_REQUEST, "Invalid request");
            }
        }

        representation = new StringRepresentation(jsonObject, MediaType.TEXT_PLAIN);

        representation.setCharacterSet(CharacterSet.UTF_8);

        return representation;
    }
}

package com.main.t2u_server.service;

import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.bean.Token;
import com.main.t2u_server.controller.Context;
import com.main.t2u_server.dataSource.IDataSource;
import com.owlike.genson.Genson;
import org.restlet.data.CharacterSet;
import org.restlet.data.MediaType;
import org.restlet.data.Status;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.ResourceException;
import org.restlet.resource.ServerResource;
import org.restlet.util.Series;

import java.util.List;

/**
 * Created by DEVELOPPEUR on 02/05/2015.
 */
public abstract class AServiceT2U extends ServerResource {
    protected Context context;

    // S�rialisation des donn�es
    Genson jsonBuilder;

    protected  ServiceAuthentication authService;

    public AServiceT2U()
    {
        this.context = Context.getCurrentContext();
        this.jsonBuilder = new Genson();
    }

    /**
     * Construire un Service avec un context autre que le courant.
     * @param _context
     */
    public AServiceT2U(Context _context) {
        super();

        this.context = _context;
    }

    /**
     * Retourne la liste des IBean depuis la base de donn�es.
     * @param entity
     * @param targetDscr
     * @return
     */
    protected Representation getLstIBean(Representation entity, IDataSource targetDscr) {
        Integer id;
        String json;
        Representation representation;
        List<IBean> listProviderUserAccount = null;

        // R�cup�ration des possibles param�tres
        try {
            //System.out.println("entity : " + entity.getText());
            id = Integer.valueOf(getQueryValue("id"));
            //id = Integer.valueOf(getQuery().getValues("id"));
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            id = null;
        }

        try {

            listProviderUserAccount = targetDscr.Select(id);

            json = jsonBuilder.serialize(listProviderUserAccount);

        } catch (Exception ex) {
            json = "{" + ex.getMessage() + "}";
        }

        representation = new StringRepresentation(json, MediaType.TEXT_PLAIN);
        representation.setCharacterSet(CharacterSet.UTF_8);

        return representation;
    }

    /**
     * Vérification du Token
     * @return
     */
    protected  Boolean checkToken(){
        Token userToken = null;
        ServiceAuthentication authService = new ServiceAuthentication();

        // R�cup�ration du Token
        try {
            Series headers = (Series) getRequestAttributes().get("org.restlet.http.headers");

            userToken = jsonBuilder.deserialize(headers.getFirstValue("Token"), Token.class);
        } catch (Exception ex) {
            throw new ResourceException(Status.CLIENT_ERROR_UNAUTHORIZED, "Invalid Token Connection refused", null);
        }

        // Vérification du Token
        if(!authService.checkTokenValidity(userToken))
            return  false;
        else
            return true;
    }
}

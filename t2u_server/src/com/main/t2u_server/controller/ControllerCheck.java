package com.main.t2u_server.controller;

import javax.swing.Timer;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControllerCheck implements ActionListener{
    private Timer oTimer;
    int delay = 5000; //milliseconds

    int iLoop = 0; //milliseconds

    public ControllerCheck()
    {
        oTimer = new Timer(delay,this);

        oTimer.start();
    }

    /**
     * obligatoire car test impl�mente l'interface ActionListener
     */
    public void actionPerformed(ActionEvent e)
    {
        iLoop = iLoop + 1;

        //quand on a cliqu� sur le bouton ici
        System.out.println("CHECK DB AND FREE SERVICES");

        if(iLoop >= 10)
            oTimer.stop();
    }
}

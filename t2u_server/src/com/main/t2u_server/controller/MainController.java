package com.main.t2u_server.controller;

import com.main.t2u_server.service.*;
import org.restlet.*;
import org.restlet.routing.Router;

// Standalone Mode
import org.restlet.service.CorsService;

import javax.servlet.ServletContext;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

/**
 * Main controller run Restlet Server REST, init CORS service and Router binding to list of services.
 * Created by DEVELOPPEUR on 06/04/2015.
 */
public class MainController extends Application {

    /**
     * Chemin relatif à l'exécution du programme
     */
    private static String relativPath;
    public static String getRelativPath() {
        return relativPath;
    }

    protected ControllerCheck oControllerCheck;

    protected CorsService corsService;

    protected Context t2uContext;

    public MainController() {
        super();

        // Initilisation des composants
        init();
    }

    private void init() {
        // Add a CORS filter to allow cross-domain requests
        corsService = new CorsService();
        //corsService.setSkippingResourceForCorsOptions(true);

        corsService.setAllowingAllRequestedHeaders(true);

        List<String> listAllowedOrigins = new ArrayList<>();

        listAllowedOrigins.add("https://webapp-t2u.rhcloud.com");
        listAllowedOrigins.add("http://webapp-t2u.rhcloud.com");
        listAllowedOrigins.add("http://127.0.0.1");
        listAllowedOrigins.add("https://127.0.0.1");

        // Local Developpment Team
        /*listAllowedOrigins.add("https://127.0.0.1/");
        listAllowedOrigins.add("http://127.0.0.1/");
        listAllowedOrigins.add("https://localhost/");
        listAllowedOrigins.add("http://localhost/");

        // OpenShift Production
        listAllowedOrigins.add("http://webapp-t2u.rhcloud.com/");
        listAllowedOrigins.add("https://webapp-t2u.rhcloud.com/");*/

        //corsService.setAllowedOrigins(new HashSet(Arrays.asList("*")));
        corsService.setAllowedOrigins(new HashSet(listAllowedOrigins));
        corsService.setAllowedCredentials(true);

        super.getServices().add(corsService);

        // Initialisation du context
        t2uContext = new Context();
    }

    /**
     * Creates a root Restlet that will receive all incoming calls.
     */
    @Override
    public synchronized Restlet createInboundRoot() {
        String relativeWebPath = "/db/T2Udb";

        ServletContext sc = (ServletContext) this.getContext().getAttributes().get("org.restlet.ext.servlet.ServletContext");
        if (sc != null) {
            relativeWebPath = sc.getRealPath(relativeWebPath);
            relativPath = relativeWebPath;
            System.out.print("#alexis#" + relativeWebPath);
        } else
            System.out.print("#alexis#" + relativeWebPath);

  /*      if(oControllerCheck == null)
            oControllerCheck = new ControllerCheck();*/

        // Create a router Restlet that routes each call to a new respective instance of resource.
        Router router = new Router(getContext());

        // Defines of routes
        router.attach("/extractbdd", ServiceFacture.class);

        router.attach("/helloworld", ServiceHelloWorld.class);

        router.attach("/account/register", SerivceAccount.class);
        router.attach("/account", SerivceAccount.class);

        router.attach("/accountlog", ServiceAccountLog.class);

        router.attach("/token", ServiceTokenProvider.class);

        router.attach("/group", ServiceGroup.class);

        router.attach("/provideruseraccount", ServiceProviderUserAccount.class);

        router.attach("/provider", ServiceProvider.class);

        router.attach("/refreshdata", ServiceRefreshData.class);

        router.attach("/chart", ServiceChart.class);

        return router;
    }
}

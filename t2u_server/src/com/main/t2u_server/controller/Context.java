package com.main.t2u_server.controller;

import com.main.t2u_server.dao.Dao;

/**
 * Created by DEVELOPPEUR on 06/06/2015.
 */
public class Context {

    public enum HostType{ OPENSHIFT, LOCAL, UNDEFINED };

    /**
     * contient la référence du dernier context instancié.
     */
    private static Context currentContext;
    public static Context getCurrentContext() {
        return currentContext;
    }

    /**
     * Permet d'exécuter des requêtes sur une base de données
     */
    private Dao dao;
    public Dao getDao() {return dao;}
    public void setDao(Dao dao) {this.dao = dao;}

    /**
     * Permet de savoir sur quel environnement le context est lancé.
     */
    private static HostType hostType;
    public static HostType getHostType() {
        if(hostType == null)
        {
            String OPENSHIFT_WILDFLY_IP = System.getenv("OPENSHIFT_WILDFLY_IP");

            if(OPENSHIFT_WILDFLY_IP != null && !OPENSHIFT_WILDFLY_IP.isEmpty())
                hostType = HostType.OPENSHIFT;
            else
                hostType = HostType.LOCAL;
        }

        return hostType;
    }

    /**
     * Construire un Context avec une initialisation automatique
     */
    public Context()
    {
        this(true);
    }

    /**
     * Construire un Context avec une initialisation automatique si withinit = true
     * @param withinit
     */
    public Context(boolean withinit)
    {
        currentContext = this;

        String OPENSHIFT_WILDFLY_IP = System.getenv("OPENSHIFT_WILDFLY_IP");

        if(OPENSHIFT_WILDFLY_IP != null && !OPENSHIFT_WILDFLY_IP.isEmpty())
            hostType = HostType.OPENSHIFT;
        else
            hostType = HostType.LOCAL;

        // R�cup�ration du Wrapper vers la base de donn�es
        dao = new Dao(withinit);
    }
}

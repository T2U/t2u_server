package com.main.t2u_server.bean;

import com.owlike.genson.annotation.JsonIgnore;
import com.owlike.genson.annotation.JsonProperty;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by DEVELOPPEUR on 18/08/2015.
 */
@Entity
@Table(name = "entetedocument")
public class EnteteDocument implements IBean{
    // Numero de l'EnteteDocument Unique Technique BDD
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_entetedocument")
    private Integer id_entetedocument = null;
    @JsonIgnore
    public Integer getId_entetedocument() {return id_entetedocument;}
    public void setId_entetedocument(Integer id_entetedocument) {this.id_entetedocument = id_entetedocument;}

    // Le compte associé au entetedocument
    @ManyToOne
    @JoinColumn(name = "id_provideruseraccount", nullable = false)
    private ProviderUserAccount provideruseraccount = null;
    @JsonIgnore
    public ProviderUserAccount getProviderUserAccount() {return provideruseraccount;}
    public void setProviderUserAccount(ProviderUserAccount provideruseraccount) {this.provideruseraccount = provideruseraccount;}

    // Liste des Lignes associ�s à l'entête
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<LineDocument> lineDocumentList = new HashSet<LineDocument>(0);
    //@JsonIgnore
    public Set<LineDocument> getLineDocumentList() {return lineDocumentList;}
    public void setLineDocumentList(Set<LineDocument> lineDocumentList) {this.lineDocumentList = lineDocumentList;}

    // Le description du entetedocument
    @Enumerated(EnumType.STRING)
    @Column(name="typedocument")
    @JsonProperty("typedocument")
    private TypeDocument typedocument = null;
    public TypeDocument getTypeDocument() {return typedocument;}
    public void setTypeDocument(TypeDocument typedocument) {this.typedocument = typedocument;}

    // Le description du entetedocument
    @Column(name="description")
    @JsonProperty("description")
    private String description = null;
    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}

//    @ManyToMany(fetch = FetchType.LAZY)
//    private Set<Chart> chartList = new HashSet<Chart>(0);
//    @JsonIgnore
//    public Set<Chart> getChartList() {return chartList;}
//    public void setChartList(Set<Chart> chartList) {this.chartList = chartList;}
    /**
     * DATE INFORMATIONS
     */
    @Column(name="updated", nullable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime updated;
    @JsonIgnore
    public LocalDateTime getUpdated() {return updated;}
    public void setUpdated(LocalDateTime updated) {this.updated = updated;}

    @Column(name="created", nullable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime created;
    @JsonIgnore
    public LocalDateTime getCreated() {return created;}
    public void setCreated(LocalDateTime created) {this.created = created;}

    @Transient
    @JsonProperty("updated")
    private String updatedString;
    public String getUpdatedString() {return updated.toString();}
    public void setUpdatedString(String updated) {this.updated = LocalDateTime.parse(updated);}

    @Transient
    @JsonProperty("created")
    private String createdString;
    public String getCreatedString() {return created.toString();}
    public void setCreatedString(String created) {this.created = LocalDateTime.parse(created);}

    public EnteteDocument(){
        // this form used by Hibernate
        updated = LocalDateTime.now();
    }

    public EnteteDocument(Integer identetedocument, ProviderUserAccount provideruseraccount, TypeDocument typedocument, String description)
    {
        this();

        this.id_entetedocument = identetedocument;
        this.setProviderUserAccount(provideruseraccount);
        this.setTypeDocument(typedocument);
        this.setDescription(description);
    }

    public enum TypeDocument {
        ORDER,
        CONSUMPTION,
        VARIOUS,
        UNDEFINED
    }
}

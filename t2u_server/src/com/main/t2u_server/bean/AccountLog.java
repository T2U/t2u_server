package com.main.t2u_server.bean;

import com.owlike.genson.annotation.JsonIgnore;
import com.owlike.genson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import javax.persistence.*;

/**
 * Account log  permet de'enregistrer les actions d'un utilisateur (connexion, refresh, ajout/suppression/modification d'un de ses comptes de fournisseur)
 * Created by DEVELOPPEUR on 06/08/2015.
 */
@Entity
@Table(name = "accountlog")
public class AccountLog implements IBean{
    // Numero du accountlog Unique Technique BDD
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_accountlog")
    private Integer id_accountlog = null;
    @JsonIgnore
    public Integer getId_accountlog() {return id_accountlog;}
    public void setId_accountlog(Integer id_accountlog) {this.id_accountlog = id_accountlog;}

    // Le compte associé au AccountLog
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_account", nullable = false)
    private Account account = null;
    @JsonIgnore
    public Account getAccount() {return account;}
    public void setAccount(Account account) {
        this.account = account;
    }

    // Le description du AccountLog
    @Enumerated(EnumType.STRING)
    @Column(name="typelog")
    @JsonProperty("typelog")
    private TypeLog typelog = null;
    public TypeLog getTypelog() {return typelog;}
    public void setTypelog(TypeLog typelog) {this.typelog = typelog;}

    // Le description du AccountLog
    @Column(name="description")
    @JsonProperty("description")
    private String description = null;
    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}

    @Column(name="updated", nullable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime updated;
    @JsonIgnore
    public LocalDateTime getUpdated() {return updated;}
    public void setUpdated(LocalDateTime updated) {this.updated = updated;}

    @Transient
    @JsonProperty("updated")
    private String updatedString;
    public String getUpdatedString() {return updated.toString();}
    public void setUpdatedString(String updated) {this.updated = LocalDateTime.parse(updated);}

    public AccountLog(){
        // this form used by Hibernate
        updated = LocalDateTime.now();
    }

    public AccountLog(Integer idAccountLog, Account account, TypeLog typelog, String description)
    {
        this();

        this.id_accountlog = idAccountLog;
        this.setAccount(account);
        this.setTypelog(typelog);
        this.setDescription(description);
    }

    public enum TypeLog {
        INFORMATION,
        WARNING,
        DANGER,
        UNDEFINED
    }
}
package com.main.t2u_server.bean;

import com.owlike.genson.annotation.JsonIgnore;
import com.owlike.genson.annotation.JsonProperty;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.sql.Blob;

import com.main.t2u_server.util.Base64;

/**
 * Created by DEVELOPPEUR on 16/06/2015.
 */
@Entity
@Table(name = "provider")
public class Provider implements IBean {

    // Numero du fournisseur Unique Technique BDD
    @Id
    private Integer id_provider = null;
    @JsonIgnore
    public Integer getId_provider() {return id_provider;}
    public void setId_provider(Integer id_provider) {this.id_provider = id_provider;}

    // Nom du Frounisseur
    @JsonProperty("name")
    private String name = null;
    public String getName() {return name;}
    public void setName(String name) {this.name = name;}

    // La Description
    @JsonProperty("description")
    private String description = null;
    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}

    // Le logo du fournisseur
    @JsonProperty("providerlogoBase64")
    private String providerlogoBase64;
    public String getProviderlogoBase64() {return providerlogoBase64;}
    public void setProviderlogoBase64(String providerlogoBase64) {this.providerlogoBase64 = providerlogoBase64;}

    private Blob providerlogo = null;
    @JsonIgnore
    public Blob getProviderlogo() {return providerlogo;}
    public void setProviderlogo(Blob providerlogo) {
        try {

            if(providerlogo != null)
                providerlogoBase64 = Base64.Encode(providerlogo.getBytes(0, (int) providerlogo.length()));
        }
        catch (Exception ex) {        }

        this.providerlogo = providerlogo;
    }

    public Provider(){
        // this form used by Hibernate
    }

    public Provider(String name, String description, Blob logo){
        this.setName(name);
        this.setDescription(description);
        this.setProviderlogo(logo);
    }
}

package com.main.t2u_server.bean;

import com.owlike.genson.annotation.JsonIgnore;
import com.owlike.genson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

/**
 * Sources <http://www.linuxcore.fr/2012/02/etcgroup/>
 * Created by DEVELOPPEUR on 10/06/2015.
 */
@Entity
@Table(name = "group_")
public class Group implements IBean{

    // Numero du groupe Unique Technique BDD
    @Id
    private Integer id_group = null;
    @JsonIgnore
    public Integer getId_group() {return id_group;}
    public void setId_group(Integer id_group) {this.id_group = id_group;}

    // Liste des Account associ�s au Group
    private Set<Account> accountList = new HashSet<Account>(0);
    @JsonIgnore
    public Set<Account> getAccountList() {return accountList;}
    public void setAccountList(Set<Account> accountList) {this.accountList = accountList;}

    // Priorit� des groupes (guid correspond � l'identifiant syst�me du groupe.)
    @JsonProperty("guid")
    private Integer guid = null;
    public Integer getGuid() {return guid;}
    public void setGuid(Integer guid) {this.guid = guid;}

    // Nom du groupe
    @JsonProperty("groupename")
    private String groupename = null;
    public String getGroupename() {return groupename;}
    public void setGroupename(String groupename) {this.groupename = groupename;}

    public Group(){
        // this form used by Hibernate
    }

    public Group(String groupename){
        this.setGroupename(groupename);
    }
}

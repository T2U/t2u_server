package com.main.t2u_server.bean;

import com.main.t2u_server.dataSource.ProviderUserAccountDataSource;
import com.owlike.genson.annotation.JsonIgnore;
import com.owlike.genson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * ATTENTION NE PAS UTILISER CETTE CLASSE (elle n'est l� que pour le temps de d�veloppement du format Ent�te, Ligne, Row)
 * Created by DEVELOPPEUR on 20/06/2015.
 */
@Deprecated
@Entity
@Table(name = "facture")
public class Facture implements IBean{

    // Numero de la facture Unique Technique BDD
    @Id
    private Integer id_facture = null;
    @JsonIgnore
    public Integer getId_facture() {return id_facture;}
    public void setId_facture(Integer id_facture) {this.id_facture = id_facture;}

    // Nom du Frounisseur
    @JsonProperty("montant")
    private Double montant = null;
    public Double getMontant() {return montant;}
    public void setMontant(Double montant) {this.montant = montant;}

    // La Description
    @JsonProperty("date")
    private String date = null;
    public String getDate() {return date;}
    public void setDate(String date) {this.date = date;}

    private ProviderUserAccount providerUserAccount = null;
    @JsonIgnore
    public ProviderUserAccount getProviderUserAccount() {return providerUserAccount;}
    public void setProviderUserAccount(ProviderUserAccount providerUserAccount) {this.providerUserAccount = providerUserAccount;}

    public Facture() {
    }
}

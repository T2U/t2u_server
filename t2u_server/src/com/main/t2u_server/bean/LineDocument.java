package com.main.t2u_server.bean;

import com.owlike.genson.annotation.JsonIgnore;
import com.owlike.genson.annotation.JsonProperty;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by DEVELOPPEUR on 18/08/2015.
 */
@Entity
@Table(name = "linedocument")
public class LineDocument  implements IBean{

    // Numero de l'LineDocument Unique Technique BDD
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_linedocument")
    private Integer id_linedocument = null;
    @JsonIgnore
    public Integer getId_linedocument() {return id_linedocument;}
    public void setId_linedocument(Integer id_linedocument) {this.id_linedocument = id_linedocument;}

    // Le compte associé a la de linedocument
//    @ManyToOne(fetch = FetchType.EAGER)
//    @JoinColumn(name = "id_entetedocument", nullable = false)
//    private EnteteDocument entetedocument = null;
//    @JsonIgnore
//    public EnteteDocument getEnteteDocument() {return entetedocument;}
//    public void setEnteteDocument(EnteteDocument entetedocument) {this.entetedocument = entetedocument;}

    // Le description du linedocument
    @Column(name="description")
    @JsonProperty("description")
    private String description = null;
    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}

    /**
     * DATE ROW
     */
    @Column(name="updated", nullable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime updated;
    @JsonIgnore
    public LocalDateTime getUpdated() {return updated;}
    public void setUpdated(LocalDateTime updated) {this.updated = updated;}

    @Column(name="created", nullable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime created;
    @JsonIgnore
    public LocalDateTime getCreated() {return created;}
    public void setCreated(LocalDateTime created) {this.created = created;}

    @Transient
    @JsonProperty("updated")
    private String updatedString;
    public String getUpdatedString() {return updated.toString();}
    public void setUpdatedString(String updated) {this.updated = LocalDateTime.parse(updated);}

    @Transient
    @JsonProperty("created")
    private String createdString;
    public String getCreatedString() {return created.toString();}
    public void setCreatedString(String created) {this.created = LocalDateTime.parse(created);}

    /**
     * CHAMP DE DOCUMENT
     */
    // La valeurXX
    @Column(name="valeur01")
    @JsonProperty("valeur01")
    private Double valeur01 = null;
    public Double getValeur01() {return valeur01;}
    public void setValeur01(Double valeur01) {this.valeur01 = valeur01;}

    // L'unité de la valeurXX
    @Column(name="unite01")
    @JsonProperty("unite01")
    private String unite01 = null;
    public String getUnite01() {return unite01;}
    public void setUnite01(String unite01) {this.unite01 = unite01;}

    // L'unité de la valeurXX
    @Column(name="libelle01")
    @JsonProperty("libelle01")
    private String libelle01 = null;
    public String getLibelle01() {return libelle01;}
    public void setLibelle01(String libelle01) {this.libelle01 = libelle01;}

    public LineDocument(){
        // this form used by Hibernate
        updated = LocalDateTime.now();
    }

    public LineDocument(Integer idlinedocument, EnteteDocument entetedocument, String description)
    {
        this();

        this.id_linedocument = idlinedocument;
        //this.setEnteteDocument(entetedocument);
        this.setDescription(description);
    }

}


package com.main.t2u_server.bean;

import com.main.t2u_server.util.AESMessage;
import com.owlike.genson.annotation.JsonIgnore;
import com.owlike.genson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Provider user's account (Ou Compte de service)
 * Created by DEVELOPPEUR on 17/06/2015.
 */
@Entity
@Table(name = "provideruseraccount")
public class ProviderUserAccount implements IBean {
    // Identifiant technique BDD d'un compte
    @Id
    private Integer id_provideruseraccount = null;
    @JsonIgnore
    public Integer getId_provideruseraccount() {return id_provideruseraccount;}
    public void setId_provideruseraccount(Integer id_provideruseraccount) {this.id_provideruseraccount = id_provideruseraccount;}

    private Account account = null;
    @JsonIgnore
    public Account getAccount() {return account;}
    public void setAccount(Account account) {
        this.account = account;
    }

    private Provider provider = null;
    //@JsonIgnore
    public Provider getProvider() {return provider;}
    public void setProvider(Provider provider) {
        this.provider = provider;
    }

    // Username de l'utilisateur
    @JsonProperty("username")
    private String username = null;
    public String getUsername() {return username;}
    public void setUsername(String username) {this.username = username;}

    // Mot de passe de l'utilisateur
    @JsonProperty("password")
    private String password = null;
    public String getPassword() {return password;}
    public void setPassword(String password) {this.password = password;}

    private AESMessage passwordv2 = null;
    @JsonIgnore
    public AESMessage getPasswordv2() {return passwordv2;}
    public void setPasswordv2(AESMessage passwordv2) {this.passwordv2 = passwordv2;}

    private AESMessage usernamev2 = null;
    @JsonIgnore
    public AESMessage getUsernamev2() {return usernamev2;}
    public void setUsernamev2(AESMessage usernamev2) {this.usernamev2 = usernamev2;}

    public ProviderUserAccount(){
        // this form used by Hibernate
    }

    public ProviderUserAccount(String username, String password)
    {
        this.setUsername(username);
        this.setPassword(password);
    }
}

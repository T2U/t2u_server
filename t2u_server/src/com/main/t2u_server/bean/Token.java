package com.main.t2u_server.bean;

import com.main.t2u_server.util.Sha256;
import com.owlike.genson.annotation.JsonIgnore;
import com.owlike.genson.annotation.JsonProperty;
import org.joda.time.DateTime;

import java.math.BigInteger;
import java.security.SecureRandom;


// H2 Hibernate
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Created by DEVELOPPEUR on 07/05/2015.
 */

@Entity
@Table(name = "token")
public class Token implements IBean {

    private static final SecureRandom random = new SecureRandom();

    private Integer refreshTokenLifeTime = 10;
    public Integer getRefreshTokenLifeTime() {return refreshTokenLifeTime;}

    // Identifiant technique d'un token
    @Id
    private Integer id_token;

    @JsonIgnore
    public Integer getId_token() {
        return id_token;
    }
    public void setId_token(Integer id_token) {
        this.id_token = id_token;
    }

    // Signature du token Unique dans la base de donn�es
    @JsonProperty("data")
    private String data;
    public String getData() {
        return data;
    }
    public void setData(String data) {
        this.data = data;
    }

    private DateTime _issuedUtc;
    @JsonProperty("issuedUtc")
    private String issuedUtc;
    public String getIssuedUtc() {
        return issuedUtc;
    }
    public void setIssuedUtc(String issuedUtc) {

        this.issuedUtc = issuedUtc;

        _issuedUtc = DateTime.parse(issuedUtc);
    }

    private DateTime _expiresUtc;

    @JsonProperty("expiresUtc")
    private String expiresUtc;
    public String getExpiresUtc() {return expiresUtc;}
    public void setExpiresUtc(String expiresUtc) {
        this.expiresUtc = expiresUtc;
        _expiresUtc = DateTime.parse(expiresUtc);
    }

    /**
     * Uuid de l'Account
     */
    private Integer uuid = null;
    @JsonProperty("uuid")
    public Integer getUuid() {return uuid;}
    public void setUuid(Integer uuid) {this.uuid = uuid;}

    private Account account = null;
    @JsonIgnore
    public Account getAccount() {return account;}
    public void setAccount(Account account) {
        this.account = account;
        if(account != null)
            this.uuid = this.account.getUuid();
    }

    public Token() {
        // this form used by Hibernate
    }

    public Token(int id_account)
    {
        account = new Account();
        account.setId_account(id_account);

        _issuedUtc = DateTime.now();
        issuedUtc = _issuedUtc.toString();

        _expiresUtc = _issuedUtc;
        _expiresUtc = _expiresUtc.plusMinutes(refreshTokenLifeTime);

        expiresUtc = _expiresUtc.toString();

        data = Sha256.getSha256(nextSessionId() + issuedUtc);

        id_token = 1;
    }

    private String nextSessionId() {
        return new BigInteger(130, random).toString(32);
    }
}

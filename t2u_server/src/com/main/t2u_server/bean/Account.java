package com.main.t2u_server.bean;

import com.owlike.genson.annotation.JsonIgnore;
import com.owlike.genson.annotation.JsonProperty;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import java.util.HashSet;
import java.util.Set;

/**
 * Srouces <http://dev.petitchevalroux.net/linux/format-etc-passwd-linux.217.html>
 */
@Entity
@Table(name = "account")
public class Account implements IBean {

    // Identifiant technique BDD d'un compte
    @Id
    private Integer id_account = null;

    @JsonIgnore
    public Integer getId_account() {return id_account;}
    public void setId_account(Integer id_account) {this.id_account = id_account;}

    // Liste des Group auquel appartient l'Account
    private Set<Group> groupList = new HashSet<Group>(0);

    @JsonIgnore
    public Set<Group> getGroupList() {return groupList;}
    public void setGroupList(Set<Group> groupList) {this.groupList = groupList;}

    // Priorit� des utilisateurs  (uuid correspond � l'identifiant syst�me de l'utilisateur.)
    @JsonProperty("uuid")
    private Integer uuid = null;
    public Integer getUuid() {return uuid;}
    public void setUuid(Integer uuid) {this.uuid = uuid;}

    // Username de l'utilisateur
    @JsonProperty("username")
    private String username = null;
    public String getUsername() {return username;}
    public void setUsername(String username) {this.username = username;}

    // Email de l'utilisateur
    @JsonProperty("email")
    private String email = null;
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }

    // Mot de passe de l'utilisateur
    @JsonProperty("password")
    private String password = null;
    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public Account(){
        // this form used by Hibernate
    }

    public Account(Integer idAccount, String username, Group primaryGroup, String email, String password)
    {
        this.id_account = idAccount;
        this.setUsername(username);
        this.setEmail(email);
        this.setPassword(password);

        this.groupList.add(primaryGroup);
    }
}

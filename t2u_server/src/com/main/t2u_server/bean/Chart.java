package com.main.t2u_server.bean;

import com.owlike.genson.annotation.JsonIgnore;
import com.owlike.genson.annotation.JsonProperty;
import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;
import org.hibernate.annotations.Type;
import org.joda.time.LocalDateTime;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by DEVELOPPEUR on 22/08/2015.
 */
@Entity
@Table(name = "chart")
public class Chart  implements IBean{

    // Numero du chart Unique Technique BDD
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="id_chart")
    private Integer id_chart = null;
    public Integer getId_chart() {return id_chart;}
    public void setId_chart(Integer id_chart) {this.id_chart = id_chart;}

    // Un chart pour un compte, Un compte pour n chart
    @ManyToOne
    @JoinColumn(name = "id_account", nullable = false)
    private Account account = null;
    @JsonIgnore
    public Account getAccount() {return account;}
    public void setAccount(Account account) {this.account = account;}

    // Liste des Document associ�s au graphique
    @ManyToMany
    @LazyCollection(LazyCollectionOption.FALSE)
    private Set<EnteteDocument> enteteDocumentList = new HashSet<EnteteDocument>(0);
    public Set<EnteteDocument> getEnteteDocumentList() {return enteteDocumentList;}
    public void setEnteteDocumentList(Set<EnteteDocument> enteteDocumentList) {this.enteteDocumentList = enteteDocumentList;}

    // Le Type de Graphique (Voir HighChartJS)
    @Enumerated(EnumType.STRING)
    @Column(name="charttype")
    @JsonProperty("charttype")
    private ChartType charttype = null;
    public ChartType getChartType() {return charttype;}
    public void setChartType(ChartType charttype) {this.charttype = charttype;}

    // Le Titre du graphique
    @Column(name="title")
    @JsonProperty("title")
    private String title = null;
    public String getTitle() {return title;}
    public void setTitle(String title) {this.title = title;}

    // Le description du graphique
    @Column(name="description")
    @JsonProperty("description")
    private String description = null;
    public String getDescription() {return description;}
    public void setDescription(String description) {this.description = description;}

    /**
     * DATE INFORMATIONS
     */
    @Column(name="updated", nullable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime updated;
    @JsonIgnore
    public LocalDateTime getUpdated() {return updated;}
    public void setUpdated(LocalDateTime updated) {this.updated = updated;}

    @Column(name="created", nullable = false)
    @Type(type="org.jadira.usertype.dateandtime.joda.PersistentLocalDateTime")
    private LocalDateTime created;
    @JsonIgnore
    public LocalDateTime getCreated() {return created;}
    public void setCreated(LocalDateTime created) {this.created = created;}

    @Transient
    @JsonProperty("updated")
    private String updatedString;
    public String getUpdatedString() {return updated.toString();}
    public void setUpdatedString(String updated) {this.updated = LocalDateTime.parse(updated);}


    @Transient
    @JsonProperty("created")
    private String createdString;
    public String getCreatedString() {return created.toString();}
    public void setCreatedString(String created) {this.created = LocalDateTime.parse(created);}

    public Chart(){
        // this form used by Hibernate
        updated = LocalDateTime.now();
    }

    public Chart(Account account, Set<EnteteDocument> enteteDocumentList, ChartType chartType, String description)
    {
        this();

        setAccount(account);
        this.setEnteteDocumentList(enteteDocumentList);
        this.setChartType(chartType);
        this.setDescription(description);
    }

    public enum ChartType {
        BASIC_LINE,
        PIE_CHART,
        UNDEFINED
    }
}

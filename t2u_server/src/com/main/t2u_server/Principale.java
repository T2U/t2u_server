package com.main.t2u_server;

//import java.sql.DatabaseMetaData;
import java.util.Arrays;
import java.util.HashSet;
import org.restlet.Application;
import org.restlet.Component;
import org.restlet.data.Protocol;
//import org.restlet.engine.application.CorsFilter;
import org.restlet.routing.Router;
import org.restlet.service.CorsService;

import com.main.t2u_server.controller.MainController;

/**
 * Main Principale en mode Developpeur
 */
public class Principale {

    public static void main(String []args)
    {
        final Router router;
        final Component component;
        final Application application;
        // final  CorsFilter corsFilter;
        CorsService corsService;

        try {
            // Add a CORS filter to allow cross-domain requests
            corsService = new CorsService();
            corsService.setAllowedOrigins(new HashSet(Arrays.asList("*")));
            corsService.setAllowedCredentials(true);

            // Create a new Component.
            component = new Component();

            // Add a new HTTP server listening on port 8182.
            component.getServers().add(Protocol.HTTP, 9001);
            router = new Router(component.getContext().createChildContext());

            // Attach the sample application.
            component.getDefaultHost().attach("/t2userver", router);
            component.getServices().add(corsService);

            application = new MainController();
            component.getDefaultHost().attach(application);

            // Start the component.
            component.start();
        }
        catch (Exception e) {
            System.out.print(e.getMessage());
        }
    }
}

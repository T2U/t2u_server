package com.main.t2u_server.dao;

// Hibernate and H2

import com.main.t2u_server.bean.*;
import com.main.t2u_server.controller.Context;
import com.main.t2u_server.controller.MainController;
import com.main.t2u_server.dataSource.ADataSource;
import com.main.t2u_server.dataSource.IDataSource;
import com.main.t2u_server.dataSource.ProviderDataSource;
import com.main.t2u_server.dataSource.ProviderUserAccountDataSource;
import com.main.t2u_server.service.SerivceAccount;
import com.main.t2u_server.service.ServiceProviderUserAccount;
import com.main.t2u_server.util.AES256;
import com.main.t2u_server.util.AESMessage;
import com.main.t2u_server.util.HibernateUtil;
import com.test.t2u_server.service.ServiceAccountTest;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.restlet.engine.header.HeaderUtils;

import javax.imageio.ImageIO;
import javax.sql.rowset.serial.SerialBlob;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.charset.Charset;
import java.sql.Blob;
import java.util.*;
import java.util.List;

/**
 * The Dao class init Hibernate parameters and manage transactions.
 */
public class Dao {

    /**
     * Contient la référence du dernier Dao instancié.
     */
    public static Dao currentDao;

    /**
     * Session Hibernate pour réaliser des transactions sur la base H2
     */
    private SessionFactory sessionFactory;

    public SessionFactory getSessionFactory() {
        return sessionFactory;
    }

    private void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private Session session;

    /**
     * Initialisation de la base de données avec un jeu de données
     */
    public Dao() {
        this(true);
    }

    /**
     * Initialisation de la base de données avec un jeu de données si withinit = true
     * @param withInit
     */
    public Dao(boolean withInit) {
        currentDao = this;

        initHibernate();

        if(withInit) {

            // DEBUG MODE populate BDD

            List<IBean> lstAccount = populateAccount();

            populateToken();

            populateGroup();

            List<IBean> lstProvider = populateProvider();

            List<IBean> listProviderUserAccounts = populateProviderUserAccount(lstAccount, lstProvider);

            try {
                IDataSource oProviderUserAccounts = new ProviderUserAccountDataSource();

                IBean objet =  new ProviderUserAccount();

                listProviderUserAccounts = oProviderUserAccounts.Select(objet);
            }
            catch (ADataSource.ADataSourceException ex)
            {

            }

            populateFacture();

            List<IBean> lstDocument = populateDocument(listProviderUserAccounts);

           List<IBean> lstChart = populateChart(lstDocument, lstAccount);
        }
    }

    /**
     * Initialisation de Hibernate
     */
    public void initHibernate() {
        //SessionFactory sessionFactory = new Configuration().configure().buildSessionFactory();
        sessionFactory = HibernateUtil.getSessionFactory();
        session = sessionFactory.openSession();
    }

    /**
     * DEBUG MODE
     */
    private void populateToken() {
        List<IBean> lstTOken = new ArrayList<>();

        for (int i = 0; i < 5; i++)
            lstTOken.add(new Token(1));

        populateBdd(lstTOken);
    }

    private void populateFacture() {
        List<IBean> lstFacture = new ArrayList<>();

        Facture anFacture;

        Random rnd = new Random();
        Double montant = 0d;
        for (int i = 0; i < 50; i++) {
            anFacture = new Facture();
            anFacture.setDate(DateTime.now().toString());

            montant = rnd.nextDouble() * rnd.nextInt();

            if(montant < 0)
                montant = montant * - 1;

            anFacture.setMontant(montant);

            ProviderUserAccount obj = new ProviderUserAccount();
            obj.setId_provideruseraccount(1);

            anFacture.setProviderUserAccount(obj);

            lstFacture.add(anFacture);
        }

        populateBdd(lstFacture);
    }

    private List<IBean> populateAccount() {
        List<IBean> lstAccount = new ArrayList<>();

        Account anAccount;

        // ROOT USER
        anAccount = new Account();
        anAccount.setUuid(0);
        anAccount.setUsername("rootAdminUser");
        anAccount.setEmail("rootAdminUser@t2u.com");
        anAccount.setPassword("53450f80c88ccd39b934d02da4075bda637466805abfa7ed3707b8170d0e1827");

        Set<Group> adds1 = new HashSet<Group>(0);

        Group rootGroup = new Group("root");
        rootGroup.setGuid(0);
        adds1.add(rootGroup);
        anAccount.setGroupList(adds1);

        lstAccount.add(anAccount);

        anAccount = new Account();
        anAccount.setUuid(501);
        anAccount.setUsername("test");
        anAccount.setEmail("test@test.com");
        anAccount.setPassword("53450f80c88ccd39b934d02da4075bda637466805abfa7ed3707b8170d0e1827");
        lstAccount.add(anAccount);

        List<IBean> lstProviderUserAccount = new ArrayList<>();

        populateAccountLog(anAccount, lstProviderUserAccount);

        anAccount = new Account();
        anAccount.setUuid(502);
        anAccount.setUsername("t2u");
        anAccount.setEmail("t2u@test.com");
        anAccount.setPassword("668138a2b8813feaf2dfa3f81af1633289be9f3aac79ad8581759c5b607ddf59");
        lstAccount.add(anAccount);
        populateAccountLog(anAccount, lstProviderUserAccount);

        anAccount = new Account();
        anAccount.setUuid(503);
        anAccount.setUsername("test02");
        anAccount.setEmail("test02@t2u.com");
        anAccount.setPassword("668138a2b8813feaf2dfa3f81af1633289be9f3aac79ad8581759c5b607ddf59");
        lstAccount.add(anAccount);
        populateAccountLog(anAccount, lstProviderUserAccount);

        anAccount = new Account();
        anAccount.setUuid(504);
        anAccount.setUsername("guillaume");
        anAccount.setEmail("guillaume@gaps.com");
        anAccount.setPassword("0a2bc34052d9f462d588dc777087c284929d1add277c6882d960cb2adab8e422");

        lstAccount.add(anAccount);
        populateAccountLog(anAccount, lstProviderUserAccount);

        populateBdd(lstAccount);

        populateBdd(lstProviderUserAccount);

        return lstAccount;
    }

    private void populateGroup() {
        List<IBean> lstGroup = new ArrayList<>();

        Group aGroup;
        Account anAccount;

        aGroup = new Group("daemon");
        aGroup.setGuid(10);
        lstGroup.add(aGroup);

        aGroup = new Group("sys");
        aGroup.setGuid(12);
        lstGroup.add(aGroup);

        aGroup = new Group("adm");
        aGroup.setGuid(5);
        lstGroup.add(aGroup);

        aGroup = new Group("basic");
        aGroup.setGuid(500);
        Set<Account> adds1 = new HashSet<Account>(0);
        anAccount = new Account();
        anAccount.setUuid(505);
        anAccount.setUsername("jean");
        anAccount.setEmail("jean@t2u.com");
        anAccount.setPassword("668138a2b8813feaf2dfa3f81af1633289be9f3aac79ad8581759c5b607ddf59");
        adds1.add(anAccount);

        anAccount = new Account();
        anAccount.setUuid(506);
        anAccount.setUsername("paul");
        anAccount.setEmail("paul@t2u.com");
        anAccount.setPassword("668138a2b8813feaf2dfa3f81af1633289be9f3aac79ad8581759c5b607ddf59");
        adds1.add(anAccount);

        anAccount = new Account();
        anAccount.setUuid(507);
        anAccount.setUsername("gaps");
        anAccount.setEmail("gaps@t2u.com");
        anAccount.setPassword("668138a2b8813feaf2dfa3f81af1633289be9f3aac79ad8581759c5b607ddf59");
        adds1.add(anAccount);

        aGroup.setAccountList(adds1);


        lstGroup.add(aGroup);

        populateBdd(lstGroup);
    }

    private List<IBean> populateProvider() {
        List<IBean> lstProvider = new ArrayList<>();
        Provider anProvider;

        System.out.println( "#alexis#" + System.getProperty("user.dir"));

        if(Context.getHostType().equals(Context.HostType.OPENSHIFT)) {

            String path = "/var/lib/openshift/5528fd93e0b8cd061300007b/app-root/runtime/repo/ressources/ProviderImg";
            //path = System.getProperty("user.dir");

            anProvider = new Provider("FREE", "ISP", BufferedImagetoBinaryData(getImage(path + "/free_logo.jpg")));
            lstProvider.add(anProvider);

            anProvider = new Provider("SFR", "ISP", BufferedImagetoBinaryData(getImage( path + "/SFR_logo.jpg")));
            lstProvider.add(anProvider);

            anProvider = new Provider("NUMERICABLE", "ISP", BufferedImagetoBinaryData(getImage(path + "/numericable_logo.jpg")));
            lstProvider.add(anProvider);

            anProvider = new Provider("ORANGE", "ISP", BufferedImagetoBinaryData(getImage(path + "/orange_logo.jpg")));
            lstProvider.add(anProvider);

            anProvider = new Provider("BOUYGUESTELECOM", "ISP", BufferedImagetoBinaryData(getImage(path + "/BOUYGUESTELECOM_logo02.jpg")));
            lstProvider.add(anProvider);
        }
        else
        {
            anProvider = new Provider("FREE", "ISP", BufferedImagetoBinaryData(getImage("./ressources/ProviderImg/free_logo.jpg")));
            lstProvider.add(anProvider);

            anProvider = new Provider("SFR", "ISP", BufferedImagetoBinaryData(getImage("./ressources/ProviderImg/SFR_logo.jpg")));
            lstProvider.add(anProvider);

            anProvider = new Provider("NUMERICABLE", "ISP", BufferedImagetoBinaryData(getImage("./ressources/ProviderImg/numericable_logo.jpg")));
            lstProvider.add(anProvider);

            anProvider = new Provider("ORANGE", "ISP", BufferedImagetoBinaryData(getImage("./ressources/ProviderImg/orange_logo.jpg")));
            lstProvider.add(anProvider);

            anProvider = new Provider("BOUYGUESTELECOM", "ISP", BufferedImagetoBinaryData(getImage("./ressources/ProviderImg/BOUYGUESTELECOM_logo02.jpg")));
            lstProvider.add(anProvider);
        }

        populateBdd(lstProvider);

        return lstProvider;
    }

    private List<IBean> populateChart(List<IBean> listDocument, List<IBean> lstAccount) {
        List<IBean> lstChart = new ArrayList<>();

        for(IBean objet2 : listDocument)
        {
            EnteteDocument anEnteteDocument = (EnteteDocument)objet2;

            Chart aNewCHart = new Chart();

            aNewCHart.setAccount(anEnteteDocument.getProviderUserAccount().getAccount());

            Set<EnteteDocument> lst = new HashSet<EnteteDocument>(0);

            lst.add(anEnteteDocument);
            aNewCHart.setAccount(anEnteteDocument.getProviderUserAccount().getAccount());
            aNewCHart.setEnteteDocumentList(lst);
            aNewCHart.setChartType(Chart.ChartType.BASIC_LINE);
            aNewCHart.setDescription("Basic Charts for " + anEnteteDocument.getProviderUserAccount().getAccount().getEmail() + " account");
            aNewCHart.setTitle(anEnteteDocument.getProviderUserAccount().getProvider().getName() + " " + anEnteteDocument.getTypeDocument());
            aNewCHart.setCreated(LocalDateTime.now());

            lstChart.add(aNewCHart);
        }

        populateBdd(lstChart);

        return lstChart;
    }

    private void populateAccountLog(Account anAccount, List<IBean> lstProviderUserAccount) {
        if(lstProviderUserAccount == null)
        lstProviderUserAccount = new ArrayList<>();

        AccountLog anAccountLog;

        anAccountLog = new AccountLog(0, anAccount, AccountLog.TypeLog.INFORMATION , " Refresh data for " + anAccount.getEmail() + " account");

        lstProviderUserAccount.add(anAccountLog);

        anAccountLog = new AccountLog(0, anAccount, AccountLog.TypeLog.DANGER , "Fatal Error when you try to delete your provider account");

        lstProviderUserAccount.add(anAccountLog);

        anAccountLog = new AccountLog(0, anAccount, AccountLog.TypeLog.UNDEFINED , "Undefined error for " + anAccount.getEmail());

        lstProviderUserAccount.add(anAccountLog);
    }

    private List<IBean> populateProviderUserAccount( List<IBean> lstAccount, List<IBean> lstProvider){
        List<IBean> lstProviderUserAccount = new ArrayList<>();
        ProviderUserAccount anProviderUserAccount;
        Account anAccount = null;
        Provider aProvider = null;

        anProviderUserAccount = new ProviderUserAccount("leusername", "lemotdepasse");
        anProviderUserAccount.setAccount((Account)lstAccount.get(0));
        anProviderUserAccount.setProvider((Provider)lstProvider.get(0));

        anProviderUserAccount.setPasswordv2(ServiceProviderUserAccount.encryptPassowrd(anProviderUserAccount));
        lstProviderUserAccount.add(anProviderUserAccount);

        for(IBean account : lstAccount)
        {
            anAccount = (Account)account;
            if(anAccount.getUuid().equals(504))
                break;
        }

        for(IBean provider : lstProvider)
        {
            aProvider = (Provider)provider;
            if(aProvider.getName().equals("FREE"))
                break;
        }

        populateBdd(lstProviderUserAccount);

        if(anAccount != null && aProvider != null)
        {
            anProviderUserAccount = new ProviderUserAccount("MYUSERNAME", "MYPASSWORD");

            anProviderUserAccount.setAccount(anAccount);
            anProviderUserAccount.setProvider(aProvider);

//            anProviderUserAccount.getAccount().setPassword("MYMAINPASSWORD"); /* inject hash*/
            try {

//                anProviderUserAccount.setUsernamev2(message);
//                anProviderUserAccount.setUsernamev2(ServiceProviderUserAccount.encryptLogin(anProviderUserAccount));
//                anProviderUserAccount.setUsername("$PROTECTED$");
//                anProviderUserAccount.setPasswordv2(ServiceProviderUserAccount.encryptPassowrd(anProviderUserAccount));
//                anProviderUserAccount.setPassword(null);
//                lstProviderUserAccount.add(anProviderUserAccount);

                session = sessionFactory.openSession();
                session.beginTransaction();
                Query query = session.createSQLQuery("INSERT INTO \"PUBLIC\".\"PROVIDERUSERACCOUNT\" (ID_PROVIDERUSERACCOUNT,ID_ACCOUNT,ID_PROVIDER,CIPHERTEXTPASSWORD,IVBYTESPASSWORD,CIPHERTEXTUSERNAME,IVBYTESUSERNAME) VALUES (2,5,1,'7M0+j8yE32tqRYRtnhMycg==','a65e8a2cb0dac8a35ffea573a48d6122','kldFklAhKpupvMv/bFdytw==','3a66615917c9cf9fc7b3e902684aa7a6')");
                query.executeUpdate();
                session.getTransaction().commit();
                session.close();
            }
            catch (Exception ex) {
                System.out.println(ex.getMessage());
            }
        }

        return lstProviderUserAccount;
    }

    private List<IBean> populateDocument(List<IBean> listProviderUserAccounts){
        List<IBean> lstEntetDocument = new ArrayList<>();

        for(IBean object : listProviderUserAccounts) {
            //ProviderUserAccount anProviderUserAccount = (ProviderUserAccount) listProviderUserAccounts.get(0);
            ProviderUserAccount anProviderUserAccount = (ProviderUserAccount)object;

            EnteteDocument oNewDocument = new EnteteDocument();
            oNewDocument.setTypeDocument(EnteteDocument.TypeDocument.CONSUMPTION);
            oNewDocument.setDescription("TEST_ENTETE");
            oNewDocument.setCreated(LocalDateTime.now());
            oNewDocument.setUpdated(LocalDateTime.now());

            Set<LineDocument> lineList = new HashSet<LineDocument>(0);
            LineDocument newLine;

            // LINE
            Double montant = 0d;
            for (int i = 0; i < 50; i++) {
                newLine = new LineDocument();

                newLine.setDescription("line#" + Integer.toString(i));
                newLine.setCreated(LocalDateTime.now());
                newLine.setUpdated(LocalDateTime.now());

                //newLine.setEnteteDocument(oNewDocument);

                montant = randDouble(0.00, 1.0) * randInt(1, 50);
                newLine.setValeur01(montant);

                newLine.setUnite01("€");
                newLine.setLibelle01("Montant");

                lineList.add(newLine);
            }

            // ENTETE
            oNewDocument.setLineDocumentList(lineList);
            oNewDocument.setProviderUserAccount(anProviderUserAccount);

            lstEntetDocument.add(oNewDocument);
        }

        populateBdd(lstEntetDocument);

        return lstEntetDocument;
    }

    private Blob BufferedImagetoBinaryData(BufferedImage obj) {
        // Store image to the table cell
        Blob imageBlob = null;
        byte[] imageInByte = null;

        try {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            ImageIO.write(obj, "jpg", baos);
            baos.flush();
            imageInByte = baos.toByteArray();

            imageBlob = new SerialBlob(imageInByte);

            //imageBlob  = new Blob(baos.toByteArray());
            baos.close();

        } catch (IOException e) {
            System.out.println(e.getMessage());
        } finally {
            return imageBlob;
        }
    }

    private BufferedImage getImage(String path) {
        BufferedImage img = null;

        try {
            FileInputStream input = new FileInputStream(path);

            img = ImageIO.read(input);
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("#alexis# " + e.getMessage());
        } finally {
            System.out.println("#alexis# GETIMAGE_PATH[" + path + "]");
            return img;
        }
    }

    private void populateBdd(List<IBean> lstBean) {
        session = sessionFactory.openSession();

        session.beginTransaction();

        for (IBean bean : lstBean) {
//            if(bean instanceof Provider) {
//                Provider otemp = (Provider) bean;
//                System.out.println("ProviderName = " + otemp.getName());
//            }
            session.save(bean);
        }

        session.getTransaction().commit();

        session.close();
    }

    // from : http://stackoverflow.com/questions/363681/generating-random-integers-in-a-range-with-java
    public static int randInt(int min, int max) {
        Random rand = new Random();

        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    // from : http://stackoverflow.com/questions/3680637/how-to-generate-a-random-double-in-a-given-range
    public static double randDouble(double min, double max) {
        Random rand = new Random();

        double randomValue = min + (max - min) * rand.nextDouble();

        return max;
    }
}

package com.main.t2u_server.dataSource;

import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.dao.Dao;
import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import java.util.List;

/**
 * Created by DEVELOPPEUR on 06/06/2015.
 */
public abstract class ADataSource implements IDataSource {

    protected Dao dao;
    protected Session session;

    public ADataSource()
    {
        // R�cup�ration du Dao courant
        dao = Dao.currentDao;
    }

    /**
     * Construire une DataSource avec un Dao autre que le courant
     * @param _dao
     */
    public ADataSource(Dao _dao)
    {
        super();

        // R�cup�ration du Dao courant
        dao = _dao;
    }

    /**
     * Ajoute un Crit�re � criteria seulement si value != de null
     * @param criteria
     * @param propertyName
     * @param value
     */
    protected boolean addRestrictionIfNotNull(Criteria criteria, String propertyName, Object value) {
        if (value != null) {
            criteria.add(Restrictions.eq(propertyName, value));
            return true;
        }
        else
            return false;
    }

    public void StartTransaction()
    {
        session = dao.getSessionFactory().openSession();
        session.beginTransaction();
    }

    public void StoptTransaction()
    {
        session.getTransaction().commit();
        session.close();
    }

    @Override
    public List<IBean> Select(String query) throws ADataSourceException {
        StartTransaction();
        List<IBean> iBeanList = Select(session.createQuery(query));
        StoptTransaction();

        return iBeanList;
    }

    @Override
    public List<IBean> Select(Query query) throws ADataSourceException {
        return query.list();
    }

    /**
     * Exception ADataSource
     */
    public class ADataSourceException extends Exception {
        public ADataSourceException(String message) {
            super(message);
        }
    }
}



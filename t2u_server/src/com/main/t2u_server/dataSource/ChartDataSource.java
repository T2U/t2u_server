package com.main.t2u_server.dataSource;

import com.main.t2u_server.bean.Chart;
import com.main.t2u_server.bean.IBean;
import org.hibernate.Criteria;

import java.util.List;

/**
 * Created by DEVELOPPEUR on 22/08/2015.
 */
public class ChartDataSource extends ADataSource {

    public ChartDataSource()
    {
        super();
    }

    @Override
    public List<IBean> Insert(IBean object) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Select(Integer idChart) throws ADataSourceException {
        List<IBean> listAccount = null;

        Chart chart = new Chart();

        if(idChart != null && idChart > 0)
            chart.setId_chart(idChart);

        listAccount = this.Select(chart);

        return listAccount;
    }

    @Override
    public List<IBean> Select(IBean object) throws ADataSourceException {
        List<IBean> listChart = null;
        Chart chart = null;
        boolean addRestriction = false;

        StartTransaction();
        Criteria criteria = session.createCriteria(Chart.class);

        if(object != null) {
            chart = (Chart) object;

            addRestriction = addRestrictionIfNotNull(criteria, "id_chart", chart.getId_chart());

            // Si pas d'identifiant unique
            if(!addRestriction) {
                if (chart.getChartType() != null)
                    addRestrictionIfNotNull(criteria, "typelog", chart.getChartType());

                if (chart.getAccount() != null) {
                    criteria.createAlias("account", "account");

                    if (!chart.getAccount().getEmail().isEmpty())
                        addRestrictionIfNotNull(criteria, "account.email", chart.getAccount().getEmail());

                    addRestrictionIfNotNull(criteria, "account.id_account", chart.getAccount().getId_account());
                    addRestrictionIfNotNull(criteria, "account.uuid", chart.getAccount().getUuid());
                }
            }
        }

        listChart = criteria.list();

        StoptTransaction();

        return listChart;
    }

    @Override
    public List<IBean> Update(IBean object) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Delete(IBean object) throws ADataSourceException {
        return null;
    }
}

package com.main.t2u_server.dataSource.provider;

import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.dataSource.ADataSource;

import java.util.List;

/**
 * Created by DEVELOPPEUR on 18/08/2015.
 */
public class AProviderWebDataSource extends ADataSource {

    public AProviderWebDataSource()
    {
        super();
    }

    @Override
    public List<IBean> Insert(IBean object) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Select(Integer id) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Select(IBean object) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Update(IBean object) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Delete(IBean object) throws ADataSourceException {
        return null;
    }
}

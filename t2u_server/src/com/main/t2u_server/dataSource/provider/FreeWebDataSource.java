package com.main.t2u_server.dataSource.provider;

import com.gargoylesoftware.htmlunit.BrowserVersion;

import com.main.t2u_server.bean.ProviderUserAccount;
import com.main.t2u_server.service.ServiceFacture;
import com.main.t2u_server.util.HttpDownloadUtility;
import com.owlike.genson.Genson;
import com.owlike.genson.GensonBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.restlet.data.CharacterSet;
import org.restlet.data.MediaType;
import org.restlet.representation.Representation;
import org.restlet.representation.StringRepresentation;
import org.restlet.resource.Get;
import org.restlet.resource.ServerResource;

import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 * Created by DEVELOPPEUR on 18/08/2015.
 */
public class FreeWebDataSource extends AProviderWebDataSource {
    private HtmlUnitDriver driver;
    private Map<String, String> data = new TreeMap<String, String>();

    private ProviderUserAccount account;

    public  void start(ProviderUserAccount account)
    {
        this.account = account;

        Logger logger = Logger.getLogger("");
        logger.setLevel(Level.OFF);
        try {
            FreeWebDataSource obj = new FreeWebDataSource();
            obj.setUp();
            obj.testFree("https://subscribe.free.fr/login/");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // http://192.168.0.18:9001/t2u_server/selenium
    public Representation handleGetSelenium(ProviderUserAccount account) {
        String json = null;
        Genson genson = new GensonBuilder().useClassMetadata(true).create();

        try{
            // Lancement de Selenium
            start(account);

            if(data.size() == 0) {
                data.put("Facture", "13€");
                data.put("Nom", "toto");
            }
            // Sérialisation des données dans un Map
            json = genson.serialize(data);
        }
        catch(Exception ex){
            System.out.println(ex.getMessage());
        }

        //return  json;

        Representation representation = new StringRepresentation(json, MediaType.TEXT_PLAIN);
        representation.setCharacterSet(CharacterSet.UTF_8);
        return representation;
    }

    public void setUp() throws Exception {
        //user-agent
        final String USER_AGENT = "Mozilla/5.0 (Windows NT 6.3; WOW64; rv:36.0) Gecko/20100101 Firefox/36.0";
        driver = new HtmlUnitDriver(new BrowserVersion("Firefox", "5.0 (Windows)", USER_AGENT, 66));
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    public void testFree(String baseUrl) throws Exception {
        driver.get(baseUrl);
        driver.findElement(By.id("login")).clear();
        driver.findElement(By.id("login")).sendKeys(account.getUsername());
        driver.findElement(By.name("pass")).clear();
        driver.findElement(By.name("pass")).sendKeys(account.getPassword());
        driver.findElement(By.name("ok")).click();

        WebDriverWait wait = new WebDriverWait(driver, 3);
        System.out.println(driver.getTitle());

        driver.findElement(By.linkText("Mon Abonnement")).click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        driver.findElement(By.linkText("Consulter mes factures")).click();
        driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);

        List<WebElement> allElements = driver.findElements(By.xpath("//div[@class='listblock accordion open']/ul/li/span"));

        // Téléchargement facture
        for (WebElement element : driver.findElementsByClassName("btn_download")) {
            if (HttpDownloadUtility.exist(".") == true ) {
                HttpDownloadUtility.downloadFile(element.getAttribute("href"), ".");
            }
        }

        int i;
        for (i=0;i<allElements.size();i++){
            System.out.println(allElements.get(i).getText());
            System.out.println(allElements.get(i+1).getText());
            ServiceFacture oService = new ServiceFacture();
            oService.creerFacture(allElements.get(i).getText(), allElements.get(i + 1).getText(),".");
            i+=2;
        }

        driver.findElement(By.linkText("e déconnecter")).click();
        System.out.println(driver.getTitle());
    }
}

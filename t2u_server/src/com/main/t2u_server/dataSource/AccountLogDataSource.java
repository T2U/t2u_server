package com.main.t2u_server.dataSource;

import com.main.t2u_server.bean.AccountLog;
import com.main.t2u_server.bean.IBean;
import org.hibernate.Criteria;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by DEVELOPPEUR on 08/08/2015.
 */
public class AccountLogDataSource  extends ADataSource {

    public AccountLogDataSource()
    {
        super();
    }

    /**
     * Selectionner un accountLog. Retourne 1 ou ALL si 1 <= 0.
     * @param idAccountLog
     * @return
     * @throws ADataSourceException
     */
    @Override
    public List<IBean> Select(Integer idAccountLog) throws ADataSourceException {
        List<IBean> listAccount = null;

        AccountLog accountLog = new AccountLog();

        accountLog.setId_accountlog(idAccountLog);

        listAccount = this.Select(accountLog);

        return listAccount;
    }

    /**
     * Poser un Account, Cr�ation d'un nouveau compte utilisateur
     * @param object
     * @return
     * @throws ADataSourceException
     */
    @Override
    public List<IBean> Insert(IBean object) throws ADataSourceException {
        List<IBean> listAccount = new ArrayList<>();
        AccountLog newAccountLog = (AccountLog)object;

        try
        {
            StartTransaction();

            session.save(newAccountLog);

            StoptTransaction();
        }
        catch (org.hibernate.exception.ConstraintViolationException ex)
        {
            throw new ADataSourceException(ex.getMessage());
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw new ADataSourceException(ex.getMessage());
        }

        listAccount.add(newAccountLog);

        return listAccount;
    }

    /**
     * S�lectionner un Bean avec un IBean de type AccountLog comme crit�re.
     * @param object
     * @return
     * @throws ADataSourceException
     */
    @Override
    public List<IBean> Select(IBean object) throws ADataSourceException {
        List<IBean> listAccountLog = null;
        AccountLog accountLog = null;
        boolean addRestriction = false;

        StartTransaction();
        Criteria criteria = session.createCriteria(AccountLog.class);

        if(object != null) {
            accountLog = (AccountLog) object;

            addRestriction = addRestrictionIfNotNull(criteria, "id_accountlog", accountLog.getId_accountlog());

            // Si pas d'identifiant unique
            if(!addRestriction) {
                if (accountLog.getTypelog() != null)
                    addRestrictionIfNotNull(criteria, "typelog", accountLog.getTypelog());

                if (accountLog.getAccount() != null) {
                    criteria.createAlias("account", "account");
                    //criteria.add(Restrictions.ilike("account.email ", "%" + providerUserAccount.getAccount().getEmail() + "%"));

                    if (!accountLog.getAccount().getEmail().isEmpty())
                        addRestrictionIfNotNull(criteria, "account.email", accountLog.getAccount().getEmail());

                    if (!accountLog.getAccount().getUsername().isEmpty())
                        addRestrictionIfNotNull(criteria, "account.username", accountLog.getAccount().getUsername());

                    addRestrictionIfNotNull(criteria, "account.uuid", accountLog.getAccount().getUuid());
                }
            }
        }

        criteria.addOrder( org.hibernate.criterion.Order.desc("updated") );

        listAccountLog = criteria.list();

        StoptTransaction();

        return listAccountLog;
    }

    /**
     * Met � jour un Compte d'utilisateur
     * @param object
     * @return
     */
    @Override
    public List<IBean> Update(IBean object) {
        return  null;
    }

    @Override
    public List<IBean> Delete(IBean object) throws ADataSourceException {
        return null;
    }
}

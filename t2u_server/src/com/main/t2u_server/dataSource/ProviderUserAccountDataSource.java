package com.main.t2u_server.dataSource;

import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.bean.ProviderUserAccount;
import org.hibernate.Criteria;
import org.hibernate.Hibernate;
import org.hibernate.criterion.Restrictions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DEVELOPPEUR on 20/06/2015.
 */
public class ProviderUserAccountDataSource extends ADataSource {

    @Override
    public List<IBean> Select(Integer idProviderUserAccount) throws ADataSourceException {
        List<IBean> listProviderUserAccount = null;

        ProviderUserAccount providerUserAccount = new ProviderUserAccount();

        providerUserAccount.setId_provideruseraccount(idProviderUserAccount);

        listProviderUserAccount = this.Select(providerUserAccount);

        return listProviderUserAccount;
    }

    @Override
    public List<IBean> Select(IBean object) throws ADataSourceException {
        List<IBean> listProviderUserAccount = null;
        ProviderUserAccount providerUserAccount = null;

        StartTransaction();
        Criteria criteria = session.createCriteria(ProviderUserAccount.class);

        if(object != null) {
            providerUserAccount = (ProviderUserAccount) object;

            addRestrictionIfNotNull(criteria, "id_provideruseraccount", providerUserAccount.getId_provideruseraccount());

            if (providerUserAccount.getAccount() != null) {
                criteria.createAlias("account", "account");
                //criteria.add(Restrictions.ilike("account.email ", "%" + providerUserAccount.getAccount().getEmail() + "%"));
                addRestrictionIfNotNull(criteria, "account.email", providerUserAccount.getAccount().getEmail());
            }

            if (providerUserAccount.getProvider() != null) {
                criteria.createAlias("provider", "provider");
                //addRestrictionIfNotNull(criteria, "provider", providerUserAccount.getProvider());
                if( !providerUserAccount.getProvider().getName().isEmpty())
                    addRestrictionIfNotNull(criteria, "provider.name", providerUserAccount.getProvider().getName());
            }
        }

        listProviderUserAccount = criteria.list();

//        for(IBean provUserAcc : listProviderUserAccount) {
//            providerUserAccount = (ProviderUserAccount)provUserAcc;
//
//            Hibernate.initialize(providerUserAccount.getAccount());
//            Hibernate.initialize(providerUserAccount.getProvider());
//
//            providerUserAccount.getAccount().getEmail();
//            providerUserAccount.getProvider().getName();
//        }

        StoptTransaction();

        return listProviderUserAccount;
    }

    /**
     * Poser un ProviderUserAccount, Cr�ation d'un nouveau compte utilisateur entre un fournisseur et un compte
     * @param object
     * @return
     * @throws ADataSourceException
     */
    @Override
    public List<IBean> Insert(IBean object) throws ADataSourceException {
        List<IBean> listProviderUserAccount = new ArrayList<>();

        ProviderUserAccount providerUserAccount = (ProviderUserAccount)object;

        try {
            StartTransaction();

            session.save(providerUserAccount);

            StoptTransaction();
        }
        catch (org.hibernate.exception.ConstraintViolationException ex) {
            throw new ADataSourceException(ex.getMessage());
        }
        catch (org.hibernate.HibernateException ex) {
            throw new ADataSourceException(ex.getMessage());
        }

        listProviderUserAccount.add(providerUserAccount);

        return listProviderUserAccount;
    }

    /**
     * Supprimer un ProviderUserAccount
     * @param object
     * @return
     * @throws ADataSourceException
     */
    @Override
    public List<IBean> Delete(IBean object) throws ADataSourceException {
        List<IBean> listProviderUserAccount = new ArrayList<>();

        ProviderUserAccount providerUserAccount;

        try {
            listProviderUserAccount = Select(object);

            if(listProviderUserAccount != null && listProviderUserAccount.size() == 1) {
                providerUserAccount = (ProviderUserAccount) listProviderUserAccount.get(0);

                StartTransaction();

                session.delete(providerUserAccount);

                StoptTransaction();
            }
            else {
                throw new ADataSourceException("Error ProviderUserAccount not found");
            }
        }
        catch (org.hibernate.exception.ConstraintViolationException ex) {
            throw new ADataSourceException(ex.getMessage());
        }
        catch (org.hibernate.HibernateException ex) {
            throw new ADataSourceException(ex.getMessage());
        }

        listProviderUserAccount.add(new ProviderUserAccount());

        return listProviderUserAccount;
    }

    /**
     * Mettre à jour un ProviderUserAccount
     * @param object
     * @return
     * @throws ADataSourceException
     */
    @Override
    public List<IBean> Update(IBean object) throws ADataSourceException {
        List<IBean> listProviderUserAccount = new ArrayList<>();

        ProviderUserAccount providerUserAccount;

        try {
            listProviderUserAccount = Select(object);

            if(listProviderUserAccount != null && listProviderUserAccount.size() == 1) {
                providerUserAccount = (ProviderUserAccount) listProviderUserAccount.get(0);

                StartTransaction();

                session.update(providerUserAccount);

                StoptTransaction();
            }
            else {
                throw new ADataSourceException("Error ProviderUserAccount not found");
            }
        }
        catch (org.hibernate.exception.ConstraintViolationException ex) {
            throw new ADataSourceException(ex.getMessage());
        }
        catch (org.hibernate.HibernateException ex) {
            throw new ADataSourceException(ex.getMessage());
        }

        listProviderUserAccount.add(providerUserAccount);

        return listProviderUserAccount;
    }
}

package com.main.t2u_server.dataSource;

import com.main.t2u_server.bean.Group;
import com.main.t2u_server.bean.IBean;
import org.hibernate.Criteria;

import java.util.List;

/**
 * Created by DEVELOPPEUR on 10/06/2015.
 */
public class GroupDataSource extends ADataSource {

    /**
     * Selectionner un Group. Retourne 1 ou ALL si 1 <= 0.
     * @param idGroup
     * @return
     * @throws ADataSourceException
     */
    @Override
    public List<IBean> Select(Integer idGroup) throws ADataSourceException {
        List<IBean> listAccount = null;

        Group group = new Group();

        group.setId_group(idGroup);

        listAccount = this.Select(group);

        return listAccount;
    }

    /**
     * S�lectionner un Bean avec un IBean de type Group comme crit�re.
     * @param object
     * @return
     * @throws ADataSourceException
     */
    @Override
    public List<IBean> Select(IBean object) throws ADataSourceException {

        List<IBean> listAccount = null;

        Group group = (Group)object;

        StartTransaction();
        Criteria criteria = session.createCriteria(Group.class);

        addRestrictionIfNotNull(criteria, "guid", group.getGuid());
        addRestrictionIfNotNull(criteria, "id_group", group.getId_group());
        addRestrictionIfNotNull(criteria, "groupename", group.getGroupename());

        listAccount = criteria.list();

        StoptTransaction();

        return listAccount;
    }

    @Override
    public List<IBean> Delete(IBean object) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Update(IBean object) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Insert(IBean object) throws ADataSourceException {
        return null;
    }
}

package com.main.t2u_server.dataSource;

import com.main.t2u_server.bean.Account;
import com.main.t2u_server.bean.IBean;
import org.hibernate.Criteria;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe de Service pour la gestion des Comptes des utilisateurs (Email/Usernam, Password)
 *
 * Created by DEVELOPPEUR on 02/05/2015.
 */
public class AccountDataSource extends ADataSource {

    public AccountDataSource()
    {
        super();
    }

    /**
     * Selectionner un Account. Retourne 1 ou ALL si 1 <= 0.
     * @param idAccount
     * @return
     * @throws ADataSourceException
     */
    @Override
    public List<IBean> Select(Integer idAccount) throws ADataSourceException {
        List<IBean> listAccount = null;

        Account account = new Account();

        account.setId_account(idAccount);

        listAccount = this.Select(account);

        return listAccount;
    }

    /**
     * Poser un Account, Cr�ation d'un nouveau compte utilisateur
     * @param object
     * @return
     * @throws ADataSourceException
     */
    @Override
    public List<IBean> Insert(IBean object) throws ADataSourceException {
        List<IBean> listAccount = new ArrayList<>();
        Account newAccount = (Account)object;

        try
        {
            StartTransaction();

            session.save(newAccount);

            StoptTransaction();
        }
        catch (org.hibernate.exception.ConstraintViolationException ex)
        {
            throw new ADataSourceException(ex.getMessage());
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw new ADataSourceException(ex.getMessage());
        }

            listAccount.add(newAccount);

            return listAccount;
    }

    /**
     * S�lectionner un Bean avec un IBean de type Account comme crit�re.
     * @param object
     * @return
     * @throws ADataSourceException
     */
    @Override
    public List<IBean> Select(IBean object) throws ADataSourceException {

        List<IBean> listAccount = null;

        Account account = (Account)object;

        StartTransaction();
        Criteria criteria = session.createCriteria(Account.class);

        addRestrictionIfNotNull(criteria, "username", account.getUsername());
        addRestrictionIfNotNull(criteria, "email", account.getEmail());
        addRestrictionIfNotNull(criteria, "uuid", account.getUuid());
        addRestrictionIfNotNull(criteria, "id_account", account.getId_account());

        listAccount = criteria.list();

        StoptTransaction();

        return listAccount;
    }

    /**
     * Met � jour un Compte d'utilisateur
     * @param object
     * @return
     */
    @Override
    public List<IBean> Update(IBean object) {
        return  null;
    }

    @Override
    public List<IBean> Delete(IBean object) throws ADataSourceException {
        return null;
    }
}

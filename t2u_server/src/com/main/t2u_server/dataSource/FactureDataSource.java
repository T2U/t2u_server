package com.main.t2u_server.dataSource;

import com.main.t2u_server.bean.Facture;
import com.main.t2u_server.bean.IBean;
import org.hibernate.Criteria;

import java.util.List;

/**
 * Created by DEVELOPPEUR on 20/06/2015.
 */
@Deprecated
public class FactureDataSource extends ADataSource{
    @Override
    public List<IBean> Insert(IBean object) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Select(Integer idFacture) throws ADataSourceException {
        List<IBean> listFacture = null;

        Facture facture = new Facture();

        facture.setId_facture(idFacture);

        listFacture = this.Select(facture);

        return listFacture;
    }

    @Override
    public List<IBean> Select(IBean object) throws ADataSourceException {
        List<IBean> listFacture = null;

        Facture facture = (Facture)object;

        StartTransaction();
        Criteria criteria = session.createCriteria(Facture.class);

        addRestrictionIfNotNull(criteria, "date", facture.getDate());
        addRestrictionIfNotNull(criteria, "id_facture", facture.getId_facture());
        addRestrictionIfNotNull(criteria, "providerUserAccount", facture.getProviderUserAccount());

        listFacture = criteria.list();

        StoptTransaction();

        return listFacture;
    }

    @Override
    public List<IBean> Update(IBean object) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Delete(IBean object) throws ADataSourceException {
        return null;
    }
}

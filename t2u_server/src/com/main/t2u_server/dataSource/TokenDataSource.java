package com.main.t2u_server.dataSource;

import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.bean.Token;
import org.hibernate.Criteria;

import java.util.ArrayList;
import java.util.List;

/**
 * Classe d'acc�s aux donn�es des Token d'un Token
 * Created by DEVELOPPEUR on 08/05/2015.
 */
public class TokenDataSource  extends ADataSource {

    public TokenDataSource()
    {
        super();
    }

    /**
     * Classical BDD SQL Query
      * @param object
     * @return
     * @throws ADataSourceException
     */
    @Override
    public List<IBean> Insert(IBean object) throws ADataSourceException {
        Token nouveauToken = (Token)object;
        List<IBean> listToken = new ArrayList<>();

        StartTransaction();

        session.save(nouveauToken);

        StoptTransaction();

        listToken.add(nouveauToken);

        return listToken;
    }

    /**
     * Hibernate BDD SQL Query
     * @param idToken
     * @return
     * @throws ADataSourceException
     */
    @Override
    public List<IBean> Select(Integer idToken) throws ADataSourceException {
        List<IBean> listToken = null;

        Token token = new Token();

        token.setId_token(idToken);

        listToken = this.Select(token);

        return listToken;
    }

    /**
     * S�lectionner un Bean avec un IBean de type Token comme crit�re.
     * @param object
     * @return
     * @throws ADataSourceException
     */
    @Override
    public List<IBean> Select(IBean object) throws ADataSourceException  {
        List<IBean> listToken = null;

        Token token = (Token)object;

        StartTransaction();
        Criteria criteria = session.createCriteria(Token.class);

        addRestrictionIfNotNull(criteria, "id_token", token.getId_token());
        addRestrictionIfNotNull(criteria, "data", token.getData());
        addRestrictionIfNotNull(criteria, "account", token.getAccount());

        listToken = criteria.list();

        StoptTransaction();

        return listToken;
    }

    /**
     * Mettre � jour les donn�es d'un Token.
     * @param object
     * @return
     * @throws ADataSourceException
     */
    @Override
    public List<IBean> Update(IBean object) throws ADataSourceException {
        Token nouveauToken = (Token)object;
        List<IBean> listToken = new ArrayList<>();

        StartTransaction();

        session.saveOrUpdate(nouveauToken);
        //session.merge(nouveauToken);

        StoptTransaction();

        listToken.add(nouveauToken);

        return listToken;
    }

    /**
     * Supprimer un Token
     * @param object
     * @return
     * @throws ADataSourceException
     */
    @Override
    public List<IBean> Delete(IBean object) throws ADataSourceException {
        Token removeToken = (Token)object;
        List<IBean> listToken = new ArrayList<>();

        StartTransaction();

        session.delete(removeToken);

        StoptTransaction();

        listToken.add(removeToken);

        return listToken;
    }
}

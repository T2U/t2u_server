package com.main.t2u_server.dataSource;

import com.main.t2u_server.bean.EnteteDocument;
import com.main.t2u_server.bean.IBean;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by DEVELOPPEUR on 18/08/2015.
 */
public class EnteteDocumentDataSource extends ADataSource {

    public EnteteDocumentDataSource()
    {
        super();
    }

    @Override
    public List<IBean> Insert(IBean object) throws ADataSourceException {
        List<IBean> listAccount = new ArrayList<>();
        EnteteDocument newEnteteDocument = (EnteteDocument)object;

        try
        {
            StartTransaction();

            session.save(newEnteteDocument);

            StoptTransaction();
        }
        catch (org.hibernate.exception.ConstraintViolationException ex)
        {
            throw new ADataSourceException("Error when you trying to insert new EnteteDocument row," + ex.getMessage());
        }
        catch (org.hibernate.HibernateException ex)
        {
            throw new ADataSourceException("Error when you trying to insert new EnteteDocument row," + ex.getMessage());
        }

        listAccount.add(newEnteteDocument);

        return listAccount;
    }

    @Override
    public List<IBean> Select(Integer id) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Select(IBean object) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Update(IBean object) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Delete(IBean object) throws ADataSourceException {
        return null;
    }
}

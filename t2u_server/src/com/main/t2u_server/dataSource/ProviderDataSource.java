package com.main.t2u_server.dataSource;

import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.bean.Provider;
import org.hibernate.Criteria;

import java.util.List;

/**
 * Created by DEVELOPPEUR on 20/06/2015.
 */
public class ProviderDataSource extends ADataSource {

    @Override
    public List<IBean> Select(Integer idProvider) throws ADataSourceException {
        List<IBean> listProvider = null;

        Provider provider = new Provider();

        provider.setId_provider(idProvider);

        listProvider = this.Select(provider);

        return listProvider;
    }

    @Override
    public List<IBean> Select(IBean object) throws ADataSourceException {
        List<IBean> listProvider = null;

        Provider provider = (Provider)object;

        StartTransaction();
        Criteria criteria = session.createCriteria(Provider.class);

        addRestrictionIfNotNull(criteria, "id_provider", provider.getId_provider());
        addRestrictionIfNotNull(criteria, "name", provider.getName());

        listProvider = criteria.list();

        StoptTransaction();

        return listProvider;
    }

    @Override
    public List<IBean> Update(IBean object) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Delete(IBean object) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Insert(IBean object) throws ADataSourceException {
        return null;
    }
}

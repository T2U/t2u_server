package com.main.t2u_server.dataSource;

import com.main.t2u_server.bean.IBean;
import org.hibernate.Query;
import java.util.List;

/**
 * Created by DEVELOPPEUR on 02/05/2015.
 */
public interface IDataSource {

    public List<IBean> Insert(IBean object) throws ADataSource.ADataSourceException;

    public List<IBean> Select(Integer id) throws ADataSource.ADataSourceException;

    public List<IBean> Select(IBean object) throws ADataSource.ADataSourceException;

    public List<IBean> Select(Query query) throws ADataSource.ADataSourceException;
    public List<IBean> Select(String query) throws ADataSource.ADataSourceException;

    public List<IBean> Update(IBean object) throws ADataSource.ADataSourceException;

    public List<IBean> Delete(IBean object) throws ADataSource.ADataSourceException;

    public void StartTransaction();

    public void StoptTransaction();
}

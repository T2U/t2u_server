package com.main.t2u_server.dataSource;

import com.main.t2u_server.bean.EnteteDocument;
import com.main.t2u_server.bean.IBean;
import com.main.t2u_server.bean.ProviderUserAccount;
import com.main.t2u_server.dataSource.provider.AProviderWebDataSource;
import com.main.t2u_server.dataSource.provider.FreeWebDataSource;
import org.joda.time.LocalDateTime;

import java.util.List;

/**
 * Created by DEVELOPPEUR on 15/08/2015.
 */
public class RefreshDataDataSource  extends ADataSource {

    public RefreshDataDataSource()
    {
        super();
    }

    @Override
    public List<IBean> Insert(IBean object) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Select(Integer id) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Select(IBean object) throws ADataSourceException {

        // En fonction du fournisseur instancier le bon DataSource
        AProviderWebDataSource dsrcFacture = new FreeWebDataSource();
        EnteteDocumentDataSource enteteDocumentDataSource = new EnteteDocumentDataSource();
        /* TODO REFRESH THE DATA FROM WEB SITE */

        //
        ProviderUserAccount oprouseracc = (ProviderUserAccount)object;

        // Inserer le nouveau document dans la base
        EnteteDocument oNewDocument = new EnteteDocument();
        oNewDocument.setTypeDocument(EnteteDocument.TypeDocument.CONSUMPTION);
        oNewDocument.setDescription("TEST");
        oNewDocument.setCreated(LocalDateTime.now());
        oNewDocument.setUpdated(LocalDateTime.now());

        oNewDocument.setProviderUserAccount(oprouseracc);

        oprouseracc.setPassword("");
        oprouseracc.setUsername("");
        oprouseracc = null;

        return enteteDocumentDataSource.Insert(oNewDocument);
    }

    @Override
    public List<IBean> Update(IBean object) throws ADataSourceException {
        return null;
    }

    @Override
    public List<IBean> Delete(IBean object) throws ADataSourceException {
        return null;
    }
}

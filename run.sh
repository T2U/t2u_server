#!/bin/bash
# @alandrieu 20-03-2015
# Team repo <https://bitbucket.org/T2U/t2u_server/overview>
# Private repo <https://github.com/alandrieu>

echo "Start Program Java t2u_server" > T2U_run.log
JAVA_HOME="/usr/lib/jvm/jdk-8-oracle-i586"
CURRENT_PATH=$(pwd)

cd t2u_server/
chmod +x gradlew
./gradlew run 2>> ../T2U_run.log
#$JAVA_HOME/jre/bin/java -classpath "$CURRENT_PATH\t2u_server\bin;$CURRENT_PATH\t2u_server\lib\restlet-jse-2.3.1\lib\org.restlet.jar;$CURRENT_PATH\t2u_server\lib\gson-2.3.1.jar;$CURRENT_PATH\t2u_server\lib\junit-4.12.jar;$CURRENT_PATH\t2u_server\lib\hamcrest-core-1.3.jar;$CURRENT_PATH\t2u_server\lib\sqlite-jdbc-3.8.7.jar;$CURRENT_PATH\t2u_server\lib\genson-0.98.jar;$CURRENT_PATH\t2u_server\lib\selenium-server-standalone-2.45.0.jar;$CURRENT_PATH\t2u_server\lib\selenium-java-2.45.0.jar" com.main.t2u_server.Principale

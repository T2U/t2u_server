# TU² Server, application Release 1.0.0

Copyright (C) 2014-2015, TU² (ISITECH Student team).

[![AppVeyor status](https://ci.appveyor.com/api/projects/status/7b6ejqi3aoygg0t0?svg=true)](https://ci.appveyor.com/project/alandrieu/t2u-server)
[![wercker status](https://app.wercker.com/status/7c31a0ca4bc6b2dfcf8b790919e21080/s "wercker status")](https://app.wercker.com/project/bykey/7c31a0ca4bc6b2dfcf8b790919e21080)
[![Codeship status](https://codeship.com/projects/16cdbd80-c251-0132-6dd8-4a57dbaf9b95/status?branch=master)](https://codeship.com/projects/73737)

------------------------------------------------------------------------
------------------------------------------------------------------------

This software is currently maintained by,

* @Alandrieu
* @Gappy

A part of TU² Team (ISITECH Student team).

If you have problems, your best bet is to post an issue to the [list](https://bitbucket.org/T2U/t2u_server/issues?status=new&status=open).

Introduction
------------
TU² Server, is an application to manage some dynamics rules. By default TU² server use H2 bdd.

* H2 [h2-1.4.187]
* Hibernate [hibernate-core-4.3.10.Final]
* Restlet [restlet-jee-2.3.1]

More information on [Wiki](https://bitbucket.org/T2U/t2u_server/wiki/Home)

Installation
------------

### Install Requirements from program source files

* Linux 
```sh
./run.sh
```

* Windows 
```bat
./run.bat
```
### Install Requirements from pre-build program

* Linux 
```sh
no Requirements
```

* Windows 
```bat
no Requirements
```

For more information please refer to [Wiki](https://bitbucket.org/T2U/t2u_server/wiki/Home)

Quick Start
-----------

### Config Files
* ./config/	contains JSON config file for application.
* ./db/ 		contains default database backup, SQL scripts.

### Start Program  from source files

* Linux 
```sh
./t2u_server/run.sh
```

* Windows 
```bat
.\t2u_server\run.bat
```

WebService Route
----------------

### Account "/account"
{FIXME}
 
### Token "/token"
{FIXME}

### Group "/group"
{FIXME}

### Provideruseraccount "/provideruseraccount"
{FIXME}
 
### Provider "/provider"
{FIXME}

For more information please refer to [Wiki](https://bitbucket.org/T2U/t2u_server/wiki/Home)

License
------------

The MIT License

Copyright (c) 2014-2015 T2U Student team, composed of Alexis Landrieu and Guillaume Appy. https://bitbucket.org/T2U

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

## CI Builds

### AppVeyor
[![AppVeyor status](https://ci.appveyor.com/api/projects/status/7b6ejqi3aoygg0t0?svg=true)](https://ci.appveyor.com/project/alandrieu/t2u-server)

### Wercker
[![wercker status](https://app.wercker.com/status/7c31a0ca4bc6b2dfcf8b790919e21080/m "wercker status")](https://app.wercker.com/project/bykey/7c31a0ca4bc6b2dfcf8b790919e21080)

### Codeship
[![Codeship status](https://codeship.com/projects/16cdbd80-c251-0132-6dd8-4a57dbaf9b95/status?branch=master)](https://codeship.com/projects/73737)